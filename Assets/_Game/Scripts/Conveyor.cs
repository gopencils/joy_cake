﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Conveyor : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public GameObject spatula;
    public Transform directionalLight;
    public Transform ground;
    public GameObject map;
    public GameObject wonder;

    [Header("Allocation Cake")]
    public float spaceBetweenPieces;

    [Space(15)]
    public int partValueMin;
    public int partValueMax;
    public int partValue;

    [Space(15)]
    public int partAmountMin;
    public int partAmountMax;
    public int partAmount;

    [Space(15)]
    // public int shortPartValueMin;
    // public int shortPartValueMax;
    public int shortPartValue;

    [Space(15)]
    public int shortPartAmountMin;
    public int shortPartAmountMax;
    public int shortPartAmount;

    #endregion

    #region PARAMS
    [Space(20)]
    public Cake currentCake;
    public int collectedCoins;
    #endregion

    #region PROPERTIES
    public static Conveyor Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        ground.gameObject.SetActive(true);
        ground.DOScaleX(3f, 1f);
        map.gameObject.SetActive(true);
        if(currentCake != null)
        currentCake.dish.transform.DOScaleY(1.3f, 0);
        Camera.main.fieldOfView = 60;
        //wonder.SetActive(true);

        if (!spatula.activeSelf)
        {
            spatula.SetActive(true);
        }

        Key.Instance.transform.SetParent(transform.parent);
        Key.Instance.HideKey();

        ClearCakes();
     //   directionalLight.rotation = Quaternion.Euler(45f, -30f, 0f);
    }

    void ClearCakes()
    {
        if (currentCake != null)
        {
            CakePool.Instance.ReturnToPool(currentCake);
        }
        currentCake = null;
    }


    public void SpawnCakes()
    {
        Refresh();

        currentCake = CakePool.Instance.GetFromPool();

        if (DataManager.Instance.IsBonusLevel())
        {
            currentCake.Initialize(7, 15, spaceBetweenPieces, 0, 0);
            currentCake.Spawn();
            // currentCake.HideTaskUI();
        }
        else
        {
            //PartValue
            partValue = partValueMin + (int)((float)DataManager.Instance.currentLevel / 5f);
            partValue = Mathf.Clamp(partValue, partValueMin, partValueMax);

            //PartAmount
            //by design, after level 10, the number of section will be 25
            if (DataManager.Instance.currentLevel <= 10f)
            {
                partAmount = partAmountMin + (int)(DataManager.Instance.currentLevel * 1.5f);
            }
            else
            {
                partAmount = (int)Random.Range(28f, 32f);
            }
            partAmount = Mathf.Clamp(partAmount, partAmountMin, partAmountMax);

            //PartShortAmount
            shortPartAmount = shortPartAmountMin + (int)((float)DataManager.Instance.currentLevel / 3f);
            shortPartAmount = Mathf.Clamp(shortPartAmount, shortPartAmountMin, shortPartAmountMax);

            //PartShortValue
            shortPartValue = (int)Random.Range(3f, 5f);

            currentCake.Initialize(partValue, partAmount, spaceBetweenPieces, shortPartValue, shortPartAmount);
            currentCake.Spawn();
            currentCake.SaveIngredientCount();
            // Debug.LogErrorFormat("Level {0} finished spawing with {1} ingredients, minimum ingredient {2}", DataManager.Instance.currentLevel, currentCake.ingredientList.Count, currentCake.minimumIngredientQuantity);
            // currentCake.SetTransformTaskUI();
        }

        currentCake.transform.SetParent(PoolLocation.Instance.cakeLocation);
        currentCake.transform.position = Vector3.forward * 5f;

    }

    public float rulerHeight;
    public void OnCompledtedLevel()
    {
        //rulerHeight = currentCake.fliedIngredientsList[currentCake.fliedIngredientsList.Count - 1].transform.position.y;
        // Debug.Log("Level Completed");
        currentCake.isCompleted = true;
        // currentCake.taskUI.ShowCheck();
        EazySoundManager.PlaySound(SoundManager.Instance.completLevel);
        StartCoroutine(C_OnCompledtedLevel());
    }

    IEnumerator C_OnCompledtedLevel()
    {
        Gate.Instance.canMove = false;
        Gate.Instance.ChangeSpeed(0f, 0.5f);
        yield return new WaitForSeconds(0.5f);

        if (DataManager.Instance.IsBonusLevel())
        {
            yield return new WaitForSeconds(0.5f);
            GameManager.Instance.WinGame();
        }
        else
        {
            spatula.GetComponent<Spatula>().Celebrate();
            yield return new WaitForSeconds(1.5f);
            spatula.SetActive(false);

            Vector3 midPos = new Vector3(0f, 0f, Gate.Instance.transform.position.z);
            currentCake.StopPhysics();
            currentCake.HideFliedIngredient();
            currentCake.dish.transform.DOMove(midPos, 1f);

            //ground.DOScaleX(200f, 1f);
            map.gameObject.SetActive(false);
            ground.gameObject.SetActive(false);
            //Camera.main.backgroundColor = Color.white;
            currentCake.SetActiveSpectator(true);
            currentCake.dish.transform.DOScaleY(currentCake.dish.transform.localScale.y * 0.4f, 0);
            //currentCake.spectatorControl.transform.parent = null;
            //currentCake.dish.transform.DOScaleY(0.3f, 0);
            //currentCake.OnWinGame();

            CameraController.Instance.LocalMove(new Vector3(10f, 6f, 0f), new Vector3(20f, -90f, 0f), 1f);
            // Camera.main.transform.DOKill();
            // Camera.main.transform.DORotateQuaternion(Quaternion.Euler(20, Camera.main.transform.eulerAngles.y, Camera.main.transform.eulerAngles.z), 1);

            directionalLight.DORotate(new Vector3(45f, -90f, 0f), 1f, RotateMode.Fast);

            yield return new WaitForSeconds(1f);

            //currentCake.SetActiveSpectator(true);
            //currentCake.spectatorControl.transform.parent = null;
            // currentCake.spectatorControl.transform.parent = currentCake.dish.transform;
            // currentCake.spectatorControl.transform.DOScaleY(currentCake.spectatorControl.transform.localScale.y / 0.4f, 0);
            currentCake.OnWinGame();

            yield return new WaitForSeconds(1.0f);

            //currentCake.OnWinGame();

            yield return new WaitForSeconds(4.5f);
        }
        //GameManager.Instance.WinGame();
        // Refresh();

    }

    #endregion

    #region DEBUG
    #endregion

}
