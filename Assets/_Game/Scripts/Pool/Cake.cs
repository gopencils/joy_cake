﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;
using System;
using Random = UnityEngine.Random;

public class Cake : MonoBehaviour
{

    #region CONST
    public const string PREF_KEY_LAST_LEVEL_INGREDIENT = "lastlevelingredient";
    public const string PREF_KEY_LAST_LEVEL = "lastlevel";
    private const float heightPerSlice = 0.22f;
    #endregion

    #region EDITOR PARAMS
    public Transform ingredientParent;
    public Transform obstacleParent;
    public MeshRenderer tableClothMeshRenderer;
    public List<Texture> tableClothTextureList;
    public Transform ruler;
    public TextMesh txtHeight;
    public BoxCollider frontDish;
    public GameObject botBread;
    // public TaskUI taskUI;
    public ParticleSystem auraPS;
    public Transform topRuler;
    public GameObject spectatorControl;
    public Transform spectatorParent;
    public GameObject cityBackground;
    public GameObject wonderObject;
    public List<GameObject> listWonderObject = new List<GameObject>();
    public List<float> listWonderHeight = new List<float>();

    #endregion

    #region PARAMS
    float spaceBetween;
    public List<Ingredient> ingredientList;
    public List<Obstacle> obstacleList;
    public List<Red> redList;
    public List<Transform> pieceList;
    public Dish dish;
    public Bread bread;
    public float currentHeight;
    public bool isCompleted;
    // public int high;
    public float rulerHeight;
    public int fliedIngredientAmount;
    public List<Ingredient> fliedIngredientsList = new List<Ingredient>();
    public int IngredientCollected { get; private set; }
    public List<Transform> textHeightList = new List<Transform>();


    public int partValue;
    public int partAmount;

    int shortPartAmount;
    int shortPartValue;

    public List<int> partList = new List<int>();

    public bool isPerfect;
    public int minimumIngredientQuantity, lastLevel;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public void Initialize(int partValue, int partAmount, float spaceBetweenPieces, int shortPartValue, int shortPartAmount)
    {
        this.partValue = partValue;
        this.partAmount = partAmount;
        this.spaceBetween = spaceBetweenPieces;
        this.shortPartValue = shortPartValue;
        this.shortPartAmount = shortPartAmount;
    }

    [NaughtyAttributes.Button]
    public void Refresh()
    {
        minimumIngredientQuantity = PlayerPrefs.GetInt(PREF_KEY_LAST_LEVEL_INGREDIENT, 0);
        lastLevel = PlayerPrefs.GetInt(PREF_KEY_LAST_LEVEL, 0);
        if (lastLevel < DataManager.Instance.currentLevel && !DataManager.Instance.IsBonusLevel())
        {
            minimumIngredientQuantity += Random.Range(1, 5);
        }
        SetActiveSpectator(false);
        ClearIngredients();
        ClearObstacles();
        ClearReds();
        IngredientCollected = 0;
        pieceList.Clear();
        isCompleted = false;
        currentHeight = 0f;
        botIngredient = null;
        ruler.gameObject.SetActive(false);
        txtHeight.gameObject.SetActive(false);
        txtHeight.text = "";
        txtHeight.transform.SetParent(transform);
        frontDish.enabled = true;
        bread.Show();
        fliedIngredientAmount = 0;
        // taskUI.HideCheck();
        partList.Clear();
        rulerHeight = 0;
        isNewHeight = false;
        posesKey.Clear();
        isPerfect = true;
        fliedIngredientsList.Clear();
    }

    void ClearIngredients()
    {
        for (int i = 0; i < ingredientList.Count; i++)
        {
            IngredientPool.Instance.ReturnToPool(ingredientList[i]);
        }
        ingredientList.Clear();
    }

    public void SaveIngredientCount()
    {
        if (lastLevel < DataManager.Instance.currentLevel)
        {
            PlayerPrefs.SetInt(PREF_KEY_LAST_LEVEL, DataManager.Instance.currentLevel);
            PlayerPrefs.SetInt(PREF_KEY_LAST_LEVEL_INGREDIENT, ingredientList.Count);
            PlayerPrefs.Save();
        }
    }

    void ClearObstacles()
    {
        for (int i = 0; i < obstacleList.Count; i++)
        {
            ObstaclePool.Instance.ReturnToPool(obstacleList[i]);
        }
        obstacleList.Clear();
    }

    void ClearReds()
    {
        for (int i = 0; i < redList.Count; i++)
        {
            RedPool.Instance.ReturnToPool(redList[i]);
        }
        redList.Clear();
    }

    Transform botIngredient;
    public List<Transform> posesKey = new List<Transform>();


    [NaughtyAttributes.Button]
    public void Spawn()
    {
        Refresh();
        // taskUI.transform.SetParent(transform, false);

        SetTableCloth();

        //Normal part
        for (int i = 0; i < partAmount; i++)
        {
            int numIngredient = UnityEngine.Random.Range(partValue - 2, partValue + 1);
            // totalIngredient += totalIngredient;
            partList.Add(numIngredient);
        }

        //Special short part
        for (int i = 0; i < shortPartAmount; i++)
        {
            partList.Add(shortPartValue);
        }

        partList.Shuffle();

        int totalIngredient = 0;
        for (int k = 0; k < partList.Count; k++)
        {
            if (totalIngredient >= minimumIngredientQuantity && totalIngredient > 100) { continue; }
            //normal section
            if (k % 2 == 0)
            {
                totalIngredient = GenerateIngredient(totalIngredient, k);
            }
            //obstacle section
            else
            {
                int numObstacle = partList[k];
                for (int i = 0; i < numObstacle; i++)
                {
                    Obstacle obstacle = ObstaclePool.Instance.GetFromPool();
                    obstacle.transform.SetParent(obstacleParent);
                    obstacle.transform.localPosition = Vector3.zero;
                    obstacle.Initialize(dish.transform);
                    obstacle.parentCake = this;
                    obstacleList.Add(obstacle);
                    pieceList.Add(obstacle.transform);
                    // obstacle.name = string.Format("Obstacle section {0}, index {1}", k, i);

                    if (DataManager.Instance.IsBonusLevel())
                    {
                        obstacle.HideObstacle();
                        obstacle.ShowRock();
                    }
                    else
                    {
                        obstacle.RandomObstacle();
                        obstacle.ShowObstacle();
                        obstacle.HideRock();
                    }
                }
            }
        }

        if (!DataManager.Instance.IsBonusLevel() && totalIngredient < minimumIngredientQuantity)
        {
            // if (lastLevel <= DataManager.Instance.currentLevel)
            // {
            int count = 0;

            int startIndex = partList.Count % 2 == 0 ? partList.Count - 2 : partList.Count - 1;
            int numLoop = 0;
            while (totalIngredient < minimumIngredientQuantity)
            {
                int index = startIndex - count;
                if (index <= 0) index = startIndex;
                Debug.LogErrorFormat("Ingredient {0} is lower than minimum {1}, regenerating by adding more to section index {2}", totalIngredient, minimumIngredientQuantity, index);
                totalIngredient = GenerateIngredient(totalIngredient, index);
                count += 2;
                if (++numLoop > 50)
                {
                    Debug.LogErrorFormat("Loop for too long, terminated, start index at {0}", startIndex);
                    break;
                }
            }
            // }
        }


        //Set Position
        Vector3 tempPos = Vector3.zero;
        for (int i = 0; i < pieceList.Count; i++)
        {
            Transform piece = pieceList[i];
            if (piece.GetComponent<Ingredient>())
            {
                if (i > 0)
                {
                    if (pieceList[i - 1].GetComponent<Ingredient>())
                    {
                        piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween);
                    }
                    else    //Obstacle --> Ingredient
                    {
                        if (DataManager.Instance.currentLevel > 20)
                        {
                            piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween * 2.75f);
                        }
                        else
                        {
                            piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween * 2.5f);
                        }
                    }
                }
            }
            else
            {
                if (i > 0)
                {
                    if (pieceList[i - 1].GetComponent<Ingredient>())    //Ingredient --> Obstacle
                    {
                        if (DataManager.Instance.currentLevel > 20)
                        {
                            piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween * 3f);
                        }
                        else
                        {
                            piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween * 2.5f);
                        }
                        posesKey.Add(piece.transform);
                    }
                    else
                    {
                        piece.transform.localPosition = tempPos + new Vector3(0f, 0f, spaceBetween * 0.5f);
                    }
                }
                Red red = RedPool.Instance.GetFromPool();
                red.transform.SetParent(piece.parent);
                red.transform.position = new Vector3(0f, 0.01f, piece.position.z);
                redList.Add(red);

            }
            tempPos = piece.transform.localPosition;
        }

        //Locate Key
        if (DataManager.Instance.currentLevel > 0 && DataManager.Instance.currentLevel % 2 == 0 && !DataManager.Instance.IsBonusLevel())
        {
            Transform keyParent = posesKey[(int)Random.Range(0, posesKey.Count)];
            Key.Instance.transform.rotation = Quaternion.Euler(0f, 0f, -45f);
            Key.Instance.transform.SetParent(keyParent);
            Key.Instance.transform.localPosition = new Vector3(0f, 0.75f, -spaceBetween * 1.75f);
            Key.Instance.ShowKey();
        }
        else
        {
            Key.Instance.HideKey();
        }

        //Locate Bread
        bread.transform.position =
        pieceList[pieceList.Count - 1].transform.position + new Vector3(0f, 0f, spaceBetween * 1.5f);

        //Locate Dish
        dish.transform.position = bread.transform.position + new Vector3(0f, 0f, spaceBetween * 3f);

        if (DataManager.Instance.IsBonusLevel())
        {
            bread.Hide();
            dish.Hide();
        }
        else
        {
            bread.Show();
            dish.Show();
        }
    }

    // private int GenerateAdditionalIngredient(int totalIngredient, int k)
    // {
    //     int maxStack = 0;
    //     for (int i = 0; i < Shop.Instance.listStack.Count; i++)
    //     {
    //         if (Shop.Instance.listStack[i].StackNumber > maxStack)
    //         {
    //             maxStack = Shop.Instance.listStack[i].StackNumber;
    //         }
    //     }

    //     for (int i = 0; i < partList[k]; i++)
    //     {
    //         if (totalIngredient > 100 && totalIngredient >= minimumIngredientQuantity)
    //         {
    //             break;
    //         }


    //     }

    //     return totalIngredient;
    // }

    private int GenerateIngredient(int totalIngredient, int k)
    {
        for (int i = 0; i < partList[k]; i++)
        {
            if (totalIngredient > 100 && totalIngredient >= minimumIngredientQuantity)
            {
                break;
            }
            int range;
            if (DataManager.Instance.currentLevel < 2)
            {
                range = 4;
            }
            else
            {
                range = Shop.Instance.listStack.Count;
            }
            Stack stack = Shop.Instance.listStack[(int)Random.Range(0, range)];

            if (DataManager.Instance.IsBonusLevel())
            {
                Ingredient ingredient = IngredientPool.Instance.GetFromPool();
                ingredient.transform.SetParent(ingredientParent);
                ingredient.transform.localPosition = Vector3.zero;
                ingredient.Initialize(dish.transform, stack.ID, stack.StackNumber);
                ingredient.HideVisual();
                ingredient.ShowCoin();
                ingredient.parentCake = this;
                ingredientList.Add(ingredient);
                pieceList.Add(ingredient.transform);
            }
            else
            {
                int lastIngredientIndex = partList.Count % 2 == 0 ? partList.Count - 2 : partList.Count - 1;
                int numberPerStack = stack.StackNumber;
                if (k == lastIngredientIndex)
                {
                    for (int s = 0; s < Shop.Instance.listStack.Count; s++)
                    {
                        if (Shop.Instance.listStack[s].StackNumber > numberPerStack)
                        {
                            numberPerStack = Shop.Instance.listStack[s].StackNumber;
                        }
                    }
                }
                for (int j = 0; j < numberPerStack; j++)
                {
                    if (totalIngredient > 100 && totalIngredient >= minimumIngredientQuantity)
                    {
                        break;
                    }
                    Ingredient ingredient = IngredientPool.Instance.GetFromPool();
                    ingredient.Initialize(dish.transform, stack.ID, stack.StackNumber);
                    ingredient.SetVisual(stack.ID);
                    if (stack.ID == Shop.Instance.LastIngredient && !DataManager.Instance.IsBonusLevel() && DataManager.Instance.currentLevel > 0 && j == stack.StackNumber - 1)
                    {
                        ingredient.ShowNewFlag();
                    }
                    else
                    {
                        ingredient.HideNewFlag();
                    }

                    ingredient.HideCoin();
                    ingredient.parentCake = this;
                    ingredientList.Add(ingredient);

                    if (j == 0)
                    {
                        botIngredient = ingredient.transform;
                        ingredient.transform.SetParent(ingredientParent);
                        ingredient.transform.localPosition = Vector3.zero;
                        pieceList.Add(ingredient.transform);
                    }
                    else
                    {
                        ingredient.transform.SetParent(botIngredient);
                        ingredient.transform.localPosition = new Vector3(0f, heightPerSlice * j, 0f);
                    }
                    if (k == lastIngredientIndex)
                    {
                        ingredient.name = "lastIngredient " + j;
                    }
                    ++totalIngredient;
                }
            }
        }

        return totalIngredient;
    }

    public void StopPhysics()
    {
        for (int i = 0; i < ingredientList.Count; i++)
        {
            ingredientList[i].StopPhysics();
        }
    }

    void SetTableCloth()
    {
        tableClothMeshRenderer.material.SetTexture("_MainTex", tableClothTextureList[(int)Random.Range(0f, tableClothTextureList.Count)]);
    }

    public void ShowRuler()
    {
        StartCoroutine(C_ShowRuler());
    }

    IEnumerator C_ShowRuler()
    {
        ruler.localScale = new Vector3(0.1f, 0f, 0.1f);
        txtHeight.text = "";
        txtHeight.transform.localScale = new Vector3(0.1f, 0.1f, 1f);

        int height = fliedIngredientAmount;
        //rulerHeight = fliedIngredientsList[fliedIngredientsList.Count - 1].transform.position.y;
        rulerHeight = fliedIngredientsList.Count * 0.1538467f;//height of 1 slice

        //if (height < 100)
        //{
        //    rulerHeight = (fliedIngredientAmount + 1.5f) * 0.15f + 0.05f;
        //}
        //else
        //{
        //    rulerHeight = (fliedIngredientAmount + 3.5f) * 0.15f + 0.05f;
        //}

        wonderObject.SetActive(true);
        for (int i = 0; i < listWonderHeight.Count; i++)
        {
            if ((i > 0 && height > listWonderHeight[i - 1] && height <= listWonderHeight[i]) || (i == 0 && height < listWonderHeight[i]))
            {
                listWonderObject[i].gameObject.SetActive(true);
            }
            else
            {
                listWonderObject[i].gameObject.SetActive(false);
            }
        }
        if (height > listWonderHeight[listWonderHeight.Count - 1])
        {
            listWonderObject[listWonderHeight.Count - 1].gameObject.SetActive(true);
        }

        // foreach(var item in textHeightList)
        // {
        //     //item.transform.parent = Camera.main.transform;
        //     item.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, item.transform.eulerAngles.y, item.transform.eulerAngles.z);
        // }
        ruler.localPosition = new Vector3(0f, 0f, 0.75f);
        ruler.gameObject.SetActive(true);
        ruler.DOScaleY(rulerHeight, 2.25f);//.OnComplete(() =>{ ruler.DOScaleY(fliedIngredientsList[fliedIngredientsList.Count - 1].transform.position.y, 0.55f); });

        txtHeight.gameObject.SetActive(true);
        txtHeight.transform.localPosition = new Vector3(0f, 1f + (0.4f / rulerHeight), 0f);

        Vector3 startCamPos = CameraController.Instance.transform.position;
        float tempX = 0f;
        float startX = startCamPos.x;
        float endX = 12f;

        float tempY = 0f;
        float startY = startCamPos.y;


        Quaternion startCamRot = CameraController.Instance.transform.localRotation;
        float tempRotX = 0f;
        float startRotX = 20f;
        float endRotX = 35f;

        float t = 0f;
        Camera.main.DOFieldOfView(90, 2);
        while (t < 1f)
        {
            txtHeight.transform.position = topRuler.position + Vector3.up * 0.5f;
            int startValue = 0;
            int endValue = height;
            t += Time.deltaTime / 2.25f;
            txtHeight.text = ((int)Mathf.Lerp(startValue, endValue, t)).ToString();

            tempX = Mathf.Lerp(startX, endX, t);

            if (startCamPos.y <= topRuler.position.y + 6f)
            {
                tempY = Mathf.Lerp(startY, topRuler.position.y + 6f, t);

                CameraController.Instance.transform.position =
                new Vector3(tempX, tempY, startCamPos.z);

                tempRotX = Mathf.Lerp(startRotX, endRotX, t);

                CameraController.Instance.transform.localRotation = Quaternion.Euler(tempRotX - 5, -90f, 0f);
            }

            yield return null;
        }

        txtHeight.text = height.ToString() + " Feet";
        Vector3 startScale = txtHeight.transform.localScale;
        txtHeight.transform
            .DOScale(startScale * 1.3f, 0.15f)
            .OnComplete(
                () =>
                {
                    txtHeight.transform.DOScale(startScale, 0.15f);
                    EazySoundManager.PlaySound(SoundManager.Instance.measureCake);
                });


        yield return new WaitForSeconds(1.5f);

        GameManager.Instance.WinGame();


    }

    public void HideFliedIngredient()
    {
        for (int i = 0; i < ingredientList.Count; i++)
        {
            if (ingredientList[i].isFlied)
            {
                ingredientList[i].HideVisual();
            }
        }
    }

    public bool isNewHeight;

    public void OnWinGame()
    {
        StartCoroutine(C_OnWinGame());
        if (isPerfect)
        {
            DailyMissionManager.Instance.perfectMission.Complete();
        }
    }

    IEnumerator C_OnWinGame()
    {
        float delta = 50;
        Vector3 startBreadLPos = bread.transform.localPosition;
        Vector3 endBreadLPos = startBreadLPos + Vector3.up * delta;
        bread.transform.localPosition = endBreadLPos;

        List<Ingredient> fliedList = new List<Ingredient>();

        for (int i = 0; i < ingredientList.Count; i++)
        {
            if (ingredientList[i].isFlied)
            {
                fliedList.Add(ingredientList[i]);
                ++IngredientCollected;
            }
        }

        DailyMissionManager.Instance.totalHeightMission.totalHeight += fliedList.Count;
        DailyMissionManager.Instance.totalHeightMission.UpdateTotalHeight();

        if (fliedList.Count > PlayerPrefs.GetInt("HIGHEST"))
        {
            isNewHeight = true;
            PlayerPrefs.SetInt("HIGHEST", fliedList.Count);
            EazySoundManager.PlaySound(SoundManager.Instance.newBest);
            DailyMissionManager.Instance.highestCakeMission.UpdateHeight(fliedList.Count);

        }
        else
        {
            // EazySoundManager.PlaySound(SoundManager.Instance.measureCake);
            isNewHeight = false;
        }

        float stepDuration = (float)(2f / (float)fliedList.Count);

        for (int i = 0; i < fliedList.Count; i++)
        {
            Ingredient flied = fliedList[i];
            Vector3 firstLPos = flied.transform.localPosition;

            flied.transform.localPosition = new Vector3(flied.transform.localPosition.x, flied.transform.localPosition.y + delta, flied.transform.localPosition.z);
        }

        ShowRuler();

        if (stepDuration <= (0.02f))
        {
            int fliedPerStep = Mathf.CeilToInt(fliedList.Count / 80f);

            for (int i = 0; i < 80f; i++)
            {
                for (int j = 0; j < fliedPerStep; j++)
                {
                    int temp = i * fliedPerStep + j;
                    if (temp >= fliedList.Count)
                    {
                        bread.Show();
                        bread.transform.DOLocalMove(startBreadLPos, 0.5f);

                        yield break;
                    }

                    Ingredient flied = fliedList[temp];
                    flied.ShowVisual();
                    Vector3 firstLPos = flied.transform.localPosition;
                    firstLPos.y -= delta;

                    flied.transform.DOLocalMove(firstLPos, 0.4f);
                    EazySoundManager.PlaySound(SoundManager.Instance.ingredientDrop);
                }
                yield return new WaitForSeconds(2f / 80f);
            }
        }
        else
        {
            for (int i = 0; i < fliedList.Count; i++)
            {
                Ingredient flied = fliedList[i];
                flied.ShowVisual();
                Vector3 firstLPos = flied.transform.localPosition;
                firstLPos.y -= delta;

                flied.transform.DOLocalMove(firstLPos, stepDuration);
                EazySoundManager.PlaySound(SoundManager.Instance.ingredientDrop);
                yield return new WaitForSeconds(stepDuration);
            }

            bread.Show();
            bread.transform.DOLocalMove(startBreadLPos, 0.5f);
        }
    }

    public void ShowAura()
    {
        StartCoroutine(C_ShowAura());
    }

    IEnumerator C_ShowAura()
    {
        auraPS.gameObject.SetActive(true);
        auraPS.Stop();
        auraPS.Play();
        yield return new WaitForSeconds(1f);
        auraPS.Stop();
        auraPS.gameObject.SetActive(false);
    }

    public void SetActiveSpectator(bool isActive)
    {
        spectatorControl.SetActive(isActive);
    }

    public void SpawnSpectator(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Vector3 randLPos = new Vector3(Random.Range(1f, 2f), 0f, Random.Range(1f, 2f));
            Spectator spectator = SpectatorPool.Instance.GetFromPool();
            spectator.transform.SetParent(spectatorParent);
            spectator.transform.localPosition = randLPos;
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
