﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Obstacle : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Rigidbody rgbd;

    public MeshFilter meshFilter;
    // public MeshRenderer meshRenderer;

    public GameObject obstaleObj;
    public List<Mesh> obstalceMeshesList;
    public GameObject rock;
    #endregion

    #region PARAMS
    public Cake parentCake;
    public Transform target;
    private bool isFlied;
    private bool isFlying;
    Tween jumpTween;
    Tween rotateTween;
    private MaterialPropertyBlock propertyBlock;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnEnable()
    {
        Refresh();
    }

    public void Initialize(Transform target)
    {
        this.target = target;
    }

    void Refresh()
    {
        StopTweens();
        StopAllCoroutines();
        isFlying = false;
        isFlied = false;
        rgbd.isKinematic = true;
        rgbd.useGravity = false;
        transform.rotation = Quaternion.identity;
        obstaleObj.transform.localPosition = Vector3.zero;
        // FastFood.Instance.ActiveFastFood(0);
    }

    void JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        StartCoroutine(C_JumpToDish(targetPos, turn, jumpPower, duration));
    }

    IEnumerator C_JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {

        float newDuration = duration;
        //newDuration = Mathf.Lerp(newDuration, 0.5f, duration);
        // CameraController.Instance.Move(new Vector3(2f, 5f, targetPos.z - 10f), duration);
        CameraController.Instance.transform
        .DOJump(new Vector3(2f, 5f, targetPos.z - 10f), 1.5f, 1, duration).SetEase(Ease.Flash);
        isFlied = true;
        isFlying = true;

        jumpTween = transform.DOJump(
            new Vector3(targetPos.x, targetPos.y + 0.3f, targetPos.z - 0.7f),
            jumpPower, 1, newDuration, false).SetEase(Ease.Flash);

        rotateTween = transform.DORotate(new Vector3(440f * turn, 0f, 0f), newDuration, RotateMode.FastBeyond360).SetEase(Ease.Flash);
        yield return new WaitForSeconds(newDuration - 0.3f);

        for (int i = 0; i < parentCake.ingredientList.Count; i++)
        {
            Ingredient ingredient = parentCake.ingredientList[i];
            if (ingredient.isFlied)
            {
                ingredient.EnablePhysics();
            }
        }

        OnJumpToDish();

        yield return new WaitForSeconds(0.1f);

        MoreMountains.NiceVibrations.MMVibrationManager.VibrateHeavy();

        for (int i = 0; i < (int)Random.Range(5f, 10f); i++)
        {
            EazySoundManager.PlaySound(SoundManager.Instance.hitCake);
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(0.2f);

        isFlying = false;
        OnJumpToDishCompleted();
    }

    void OnJumpToDish()
    {
        rgbd.isKinematic = false;
        rgbd.useGravity = true;
        rgbd.AddForce(0f, 1000f, 5000f);
    }

    void OnJumpToDishCompleted()
    {
        StartCoroutine(C_OnJumpToDishCompleted());
    }

    IEnumerator C_OnJumpToDishCompleted()
    {
        yield return new WaitForSeconds(1f);
        if (DataManager.Instance.IsBonusLevel())
        {
            Conveyor.Instance.Refresh();
            GameManager.Instance.WinGame();
        }
        else
        {
            GameManager.Instance.LoseGame();
        }
    }

    void JumpOut(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        CoinPS coinPS = CoinPSPool.Instance.GetFromPool();
        coinPS.SetCoin();
        coinPS.transform.SetParent(PoolLocation.Instance.coinPSParentWG, false);
        coinPS.transform.localPosition = new Vector3(Random.Range(-40f, 40f), Random.Range(-40f, 40f), 0f);
        coinPS.MoveToCorner(PoolLocation.Instance.coinPSEndPos.position, Vector3.one, Vector3.one * 0.5f, 0.5f);

        DataManager.Instance.currentCoin++;
        UIManager.Instance.UpdateCoin();

        StartCoroutine(C_JumpOut(targetPos, turn, jumpPower, duration));
    }

    IEnumerator C_JumpOut(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        yield return new WaitForSeconds(0.08f);
        isFlying = true;
        jumpTween = transform.DOJump(targetPos, jumpPower, 1, duration, false);
        rotateTween = transform.DORotate(new Vector3(0f, 0f, 360f * turn), 1, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(duration);
        isFlying = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        float duration;
        if (DataManager.Instance.currentLevel < 10)
        {
            duration = 1.5f;
        }
        else
        {
            duration = 3f;
        }
        if (!isFlying && !isFlied)
        {
            // if (Gate.Instance.isFevering)
            // {
            //     if (other.CompareTag("Gate1") || other.CompareTag("Gate0"))
            //     {
            //         JumpOut(transform.position + Vector3.left * 7f, 1, 4f, 2f);
            //         Gate.Instance.spatula.Flick();
            //     }
            // }
            // else
            // {
            if (other.CompareTag("Gate1"))
            {
                SoundManager.Instance.PlayFailSounds(1);
                JumpToDish(target.transform.position, 1, 5f, duration);
                Gate.Instance.spatula.Flip();
                Gate.Instance.ChangeSpeed(0f, 1.5f);
                Gate.Instance.canMove = false;
                EazySoundManager.PlaySound(SoundManager.Instance.flip);

                // Gate.Instance.StopCheckFever();
                // UIManager.Instance.UpdateImgFever(0f);
                Conveyor.Instance.currentCake.isPerfect = false;
            }
            if (other.CompareTag("Gate0"))
            {
                JumpOut(transform.position + Vector3.left * 7f, 1, 4f, 2f);
                Gate.Instance.spatula.Flick();
            }
            // }
        }

    }

    public void StopTweens()
    {
        jumpTween.Kill(false);
        rotateTween.Kill(false);
    }

    public void RandomObstacle()
    {
        Mesh randMesh = obstalceMeshesList[(int)Random.Range(0f, obstalceMeshesList.Count)];
        meshFilter.mesh = randMesh;
        float heightMesh = randMesh.bounds.size.y;
        obstaleObj.transform.localPosition = new Vector3(0f, heightMesh / 2f, 0f);
    }


    public void HideObstacle()
    {
        obstaleObj.SetActive(false);
    }

    public void ShowObstacle()
    {
        obstaleObj.SetActive(true);
    }

    public void ShowRock()
    {
        rock.SetActive(true);
    }

    public void HideRock()
    {
        rock.SetActive(false);
    }

    #endregion

    #region DEBUG
    #endregion

}
