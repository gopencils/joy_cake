﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CoinPS : MonoBehaviour
{

    public Sprite coinSprite;
    public Sprite keySprite;

    public Image image;

    public void SetCoin()
    {
        image.sprite = coinSprite;
    }

    public void SetKey()
    {
        image.sprite = keySprite;
    }

    public void MoveToCorner(Vector3 targetPos, Vector3 startScale, Vector3 endScale, float duration = 1f)
    {
        transform.localScale = startScale;
        transform.DOScale(endScale, duration).SetEase(Ease.InQuad);
        transform.DOMove(targetPos, duration).SetEase(Ease.Flash).OnComplete(() => { CoinPSPool.Instance.ReturnToPool(this); });
    }

}
