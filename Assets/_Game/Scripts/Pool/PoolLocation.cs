﻿using UnityEngine;

public class PoolLocation : MonoBehaviour
{
    public static PoolLocation Instance { get; private set; }

    public Transform cakeLocation;

    public Transform coinPSParentWG;
    public Transform coinPSParent;
    public Transform coinPSStartPos;
    public Transform coinPSEndPos;

    private void Awake()
    {
        Instance = this;
    }
}
