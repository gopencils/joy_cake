﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Red : MonoBehaviour
{

    public void DelayReturnToPool()
    {
        StartCoroutine(C_DelayReturnToPool());
    }

    IEnumerator C_DelayReturnToPool()
    {
        yield return new WaitForSeconds(2f);
        RedPool.Instance.ReturnToPool(this);
    }
}
