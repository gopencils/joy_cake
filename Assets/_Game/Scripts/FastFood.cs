﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastFood : MonoBehaviour
{
    public void Start()
    {
        RandomScale();
    }

    private void RandomScale()
    {
        float _value = Random.Range(0.6f, 1.6f);
        transform.localScale = Vector3.one * _value;

        if (Random.value > 0.8f)
        {
            gameObject.SetActive(false);
        }

        if (transform.position.x < 10 || transform.position.x > -10)
        {
        //    transform.localScale = Vector3.one * 3.0f;

        }
    }
}
