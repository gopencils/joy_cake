﻿using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("Sources")]
    public AudioClip flip;
    public AudioClip flick;
    public AudioClip completeCake;
    public AudioClip completeCakeClap;
    public AudioClip completLevel;
    public AudioClip coinsCollect;
    public AudioClip rewardClaimed;
    public AudioClip newBest;
    public AudioClip fail;
    public AudioClip measureCake;
    public AudioClip winGame;
    public AudioClip ingredientDrop;
    public AudioClip hitCake;
    public AudioClip buttonClick;
    public AudioClip upgradeClicked;
    public AudioClip unlockIngredient;
    public AudioClip popupOpen;
    public AudioClip popupClose;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static SoundManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS
    private void Awake()
    {
        Instance = this;
    }

    public void PlayFailSounds(int count)
    {
        StartCoroutine(C_PlayFailSounds(count));
    }

    IEnumerator C_PlayFailSounds(int count)
    {
        for (int i = 0; i < count; i++)
        {
            EazySoundManager.PlaySound(fail);
            yield return new WaitForSeconds(0.2f);
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
