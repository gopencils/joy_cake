﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TaskUI : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Image imgCake;
    public Image imgProgressFill;
    public GameObject check;

    // public List<Sprite> cakeSpriteList;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    // public void RandomCakeSprite()
    // {
    //     imgCake.sprite = cakeSpriteList[(int)Random.Range(0, cakeSpriteList.Count)];
    // }

    private void OnEnable()
    {
        imgProgressFill.fillAmount = 0f;
    }

    public void UpdateProgress(float percent)
    {
        imgProgressFill.fillAmount = percent;
        // StartCoroutine(C_UpdateProgress(percent));
    }

    // IEnumerator C_UpdateProgress(float percent)
    // {
    //     float start = imgProgressFill.fillAmount;
    //     float t = 0f;
    //     while (t < 1f)
    //     {
    //         t += Time.deltaTime / 0.5f;
    //         imgProgressFill.fillAmount = Mathf.Lerp(imgProgressFill.fillAmount, percent, t);
    //         yield return null;
    //     }
    // }

    public void ShowCheck()
    {
        check.SetActive(true);

        Vector3 startScale = transform.localScale;
        transform.DOScale(startScale * 1.2f, 0.25f).OnComplete(() => { transform.DOScale(startScale, 0.25f); });
    }

    public void HideCheck()
    {
        check.SetActive(false);
    }

    #endregion

    #region DEBUG
    #endregion

}
