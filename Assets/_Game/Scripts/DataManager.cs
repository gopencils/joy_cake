﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string COIN = "COIN";
    public const string KEY = "KEY";
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public int currentLevel;
    public int currentCoin;
    public int currentKey;
    #endregion

    #region PROPERTIES
    public static DataManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        LoadData();
        // ResetData();
        //PlayerPrefs.SetInt(COIN, 99999);
        currentCoin = PlayerPrefs.GetInt(COIN);
        // PlayerPrefs.SetInt(LEVEL, 21 );
        // currentLevel = PlayerPrefs.GetInt(LEVEL);
        // PlayerPrefs.SetInt(KEY, 2);
        // currentLevel = PlayerPrefs.GetInt(KEY);
    }

    public bool IsBonusLevel()
    {
        if (DataManager.Instance.currentLevel % 5 == 0 && DataManager.Instance.currentLevel > 0)
            return true;
        else
            return false;
    }

    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        currentCoin = PlayerPrefs.GetInt(COIN);
        currentKey = PlayerPrefs.GetInt(KEY);
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(LEVEL, currentLevel);
        PlayerPrefs.SetInt(COIN, currentCoin);
        PlayerPrefs.SetInt(KEY, currentKey);
    }

    public void ResetData()
    {
        PlayerPrefs.SetInt(LEVEL, 0);
        PlayerPrefs.SetInt(COIN, 0);
        PlayerPrefs.SetInt(KEY, 0);

        SaveData();
    }

    public void ChangeCoin(int offset)
    {
        currentCoin += offset;
        SaveData();
        UIManager.Instance.UpdateCoin();
    }

    public void ChangeKey(int offset)
    {
        currentKey += offset;
        currentKey = Mathf.Clamp(currentKey, 0, 3);
        SaveData();
        UIManager.Instance.UpdateKeys(currentKey);
    }

    public void CheckKeys()
    {
        if (currentKey >= 3)
        {
            Shop.Instance.ShowPopup3Keys();
        }
    }

    [NaughtyAttributes.Button]
    public void Delete()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
    #endregion
}
