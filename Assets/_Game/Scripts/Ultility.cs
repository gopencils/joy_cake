﻿public static class Ultility
{
    public static string FormatNumber(int num)
    {
        if (num >= 1000000000)
            return FormatNumber(num / 1000000000) + "B";
        if (num >= 1000000)
            return FormatNumber(num / 1000000) + "M";
        if (num >= 100000)
            return FormatNumber(num / 1000) + "K";
        if (num >= 10000)
            return (num / 1000D).ToString("0.#") + "K";
        if (num >= 1000)
            return (num / 1000D).ToString("0.#") + "K";
        return num.ToString("#,0");
    }
}
