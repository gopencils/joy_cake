﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Hellmade.Sound;

public class UIManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("SCREENS")]
    // public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;
    public GameObject map;
    // public GameObject ITEM;


    // [Header("MENU")]
    // public Button btnIngredients;
    // public Button btnStack;
    // public GameObject tutorial;

    [Header("WINGAME")]
    // public Text txtTallest;
    public Text txtCoinWingame;
    public Button btnCollect;
    // public Text lblTallest;
    public Text lblTotalHeight;
    public Text txtTotalHeight;

    [Header("ONTOP")]
    public Text txtCoin;

    [Header("INGAME")]
    public Text txtLevel;
    public Image imgCircle;
    public GameObject lblBonus;
    public Transform tasksParent;
    public Text txtTutorialHold;
    public Text txtTutorialRelease;
    // public Image imgFever;
    // public Text lblFever;
    public List<GameObject> keysList;

    // public GameObject feverGO;
    // public Text txtFever;


    // [Header("ITEMS")]
    public Button btnBack;
    #endregion

    #region PARAMS
    public static UIManager Instance { get; private set; }

    int currentCoin;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    public void OnClick_ActiveMap()
    {
        map.SetActive(!map.activeSelf);
    }
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
        // btnIngredients.onClick.AddListener(OnBtnIngredientClicked);
        // btnStack.onClick.AddListener(OnBtnStackClicked);
        //      btnBack.onClick.AddListener(OnBtnBackClicked);
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
        // btnIngredients.onClick.RemoveListener(OnBtnIngredientClicked);
        // btnStack.onClick.RemoveListener(OnBtnStackClicked);
        //    btnBack.onClick.RemoveListener(OnBtnBackClicked);
    }

    // void OnBtnIngredientClicked()
    // {
    //     ITEM.SetActive(true);
    //     ItemManager.Instance.ShowIngredient();
    // }

    // void OnBtnStackClicked()
    // {
    //     ITEM.SetActive(true);
    //     ItemManager.Instance.ShowStack();
    // }

    public void OnBtnBackClicked()
    {
        Conveyor.Instance.SpawnCakes();
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            // case GameState.WINGAME:
            //     OnWinGame();
            // break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        // Menu.SetActive(true);
        Shop.Instance.ShowMainUI();
        // InGame.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
        // ITEM.SetActive(false);

        UpdateCoin();
        UpdateLevel();
        lblBonus.SetActive(false);
        imgCircle.fillAmount = 0f;
        tempCoin = 0;

        UpdateKeys(DataManager.Instance.currentKey);
        Shop.Instance.UpdatePrice();
        // if (DataManager.Instance.currentLevel == 0)
        // {
        //     tutorial.SetActive(true);
        // }
        // else
        // {
        //     tutorial.SetActive(false);
        // }

    }

    public void OnInGame()
    {
        // Menu.SetActive(false);
        Shop.Instance.HideMainUI();
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);

        imgCircle.fillAmount = 0f;
        // UpdateKeys(DataManager.Instance.currentKey);

        if (DataManager.Instance.IsBonusLevel())
        {
            lblBonus.SetActive(true);
        }
        else
        {
            lblBonus.SetActive(false);
        }
    }

    public void OnWinGame()
    {
        // Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);

        UpdateWinGame();
        lblBonus.SetActive(false);
        btnCollect.interactable = true;
        EazySoundManager.PlaySound(SoundManager.Instance.winGame);

        // Shop.Instance.ShowComplete(Conveyor.Instance.currentCake.high, tempCoin)
    }

    public void OnLoseGame()
    {
        // Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
        lblBonus.SetActive(false);
    }

    public int tempCoin;

    public void UpdateWinGame()
    {
        if (DataManager.Instance.IsBonusLevel())
        {
            tempCoin = (int)(Conveyor.Instance.collectedCoins * 5.5f);
            tempCoin *= Mathf.FloorToInt(Mathf.Pow(5, DataManager.Instance.currentLevel / 10));
        }
        else
        {
            tempCoin = (int)(Conveyor.Instance.collectedCoins * 5.5f);
        }

        txtCoinWingame.text = tempCoin.ToString();
        txtTotalHeight.text = "";
        txtTotalHeight.gameObject.SetActive(false);
        lblTotalHeight.gameObject.SetActive(false);
        // }
        // else
        // {
        //     tempCoin = (int)(Conveyor.Instance.currentCake.fliedIngredientAmount) * 3;

        //     txtCoinWingame.text = tempCoin.ToString();
        //     txtTotalHeight.text = ((int)(Conveyor.Instance.currentCake.fliedIngredientAmount)).ToString() + " Feet";
        //     txtTotalHeight.gameObject.SetActive(true);
        //     lblTotalHeight.gameObject.SetActive(true);
        // }

    }

    public void Collect()
    {
        btnCollect.interactable = false;
        EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        StartCoroutine(C_Collect());
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
    }


    IEnumerator C_Collect()
    {
        currentCoin = tempCoin;

        int level = PlayerPrefs.GetInt("LEVEL");
        int bonusValue = (level + 1) / 10;
        if (bonusValue < 1 || DataManager.Instance.IsBonusLevel())
        {
            bonusValue = 1;
        }
        DataManager.Instance.ChangeCoin(currentCoin * bonusValue);
        txtCoinWingame.DOText("0", 1f, true, ScrambleMode.Numerals);
        SpawnCoinPS(btnCollect.transform);
        yield return new WaitForSeconds(1f);
        GameManager.Instance.StartGame();
        tempCoin = 0;
    }

    public void SpawnCoinPS()
    {
        for (int i = 0; i < (int)Random.Range(15f, 20f); i++)
        {
            CoinPS coinPS = CoinPSPool.Instance.GetFromPool();
            coinPS.SetCoin();
            coinPS.transform.SetParent(PoolLocation.Instance.coinPSParent, false);
            coinPS.transform.localPosition = new Vector3(Random.Range(-40f, 40f), Random.Range(-40f, 40f), 0f);
            coinPS.MoveToCorner(PoolLocation.Instance.coinPSEndPos.position, Vector3.one, Vector3.one * 0.5f);
        }
    }

    public void SpawnCoinPS(Transform startTrans)
    {
        for (int i = 0; i < (int)Random.Range(15f, 20f); i++)
        {
            CoinPS coinPS = CoinPSPool.Instance.GetFromPool();
            coinPS.SetCoin();
            coinPS.transform.SetParent(startTrans, false);
            coinPS.transform.localPosition = new Vector3(Random.Range(-40f, 40f), Random.Range(-40f, 40f), 0f);
            coinPS.MoveToCorner(PoolLocation.Instance.coinPSEndPos.position, Vector3.one, Vector3.one * 0.5f);
        }
    }

    public void UpdateCoin()
    {
        txtCoin.DOText(Ultility.FormatNumber(DataManager.Instance.currentCoin), 0.3f, true, ScrambleMode.Numerals);
        txtCoin.transform.DOScale(Vector3.one * 1.3f, 0.15f).
        OnComplete(() => { txtCoin.transform.DOScale(Vector3.one, 0.15f); });
    }

    public void UpdateLevel()
    {
        txtLevel.text = (DataManager.Instance.currentLevel + 1).ToString();
        Shop.Instance.levelIngredientText.text = "LEVEL " + (DataManager.Instance.currentLevel + 1).ToString();
    }

    public void UpdateLevelCircle(float percent)
    {
        StartCoroutine(C_UpdateLevelCircle(percent));
    }

    IEnumerator C_UpdateLevelCircle(float percent)
    {
        float start = imgCircle.fillAmount;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 1f;
            imgCircle.fillAmount = Mathf.Lerp(start, percent, t);
            yield return null;
        }
    }

    public void ShowTutorialHold()
    {
        txtTutorialHold.gameObject.SetActive(true);
    }

    public void HideTutorialHold()
    {
        txtTutorialHold.gameObject.SetActive(false);
    }


    public void ShowTutorialRelease()
    {
        txtTutorialRelease.gameObject.SetActive(true);
    }

    public void HideTutorialRelease()
    {
        txtTutorialRelease.gameObject.SetActive(false);
    }

    // public void UpdateImgFever(float percent)
    // {
    //     imgFever.fillAmount = percent;
    // }

    // public void UpdateFevering(float duration)
    // {
    //     StartCoroutine(C_UpdateFevering(duration));
    // }

    // IEnumerator C_UpdateFevering(float duration)
    // {
    //     feverGO.SetActive(true);
    //     txtFever.text = duration.ToString();
    //     while (duration > 0f)
    //     {
    //         yield return new WaitForSeconds(1f);
    //         txtFever.text = (--duration).ToString();
    //         txtFever.transform.DOScale(Vector3.one * 1.3f, 0.15f)
    //                 .OnComplete(() => { txtFever.transform.DOScale(Vector3.one, 0.15f); });
    //     }
    //     txtFever.text = "";
    //     feverGO.SetActive(false);
    // }

    // public void UpdateLblFever(float alpha)
    // {
    //     lblFever.color = new Color(1f, 0.4f, 0f, alpha);
    // }

    public void UpdateKeys(int currentKeys)
    {
        if (currentKeys > 3)
        {
            return;
        }

        if (currentKeys > 0 && GameManager.Instance.currentState == GameState.INGAME)
        {
            CoinPS coinPS = CoinPSPool.Instance.GetFromPool();
            coinPS.SetKey();
            coinPS.transform.SetParent(PoolLocation.Instance.coinPSParentWG, false);
            coinPS.transform.localPosition = new Vector3(Random.Range(-40f, 40f), Random.Range(-40f, 40f), 0f);
            coinPS.MoveToCorner(keysList[currentKeys - 1].transform.position, Vector3.one * 1.5f, Vector3.one, 0.5f);
        }

        for (int i = 0; i < keysList.Count; i++)
        {
            Transform key = keysList[i].transform;
            if (i < currentKeys)
            {
                key.gameObject.SetActive(true);
                key.DOScale(Vector3.one * 1.3f, 0.25f)
                    .OnComplete(() => { key.DOScale(Vector3.one, 0.25f); });
            }
            else
            {
                key.gameObject.SetActive(false);
            }
        }
    }

    #endregion
}