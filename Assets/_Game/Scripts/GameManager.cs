﻿using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public GameState currentState;
    #endregion

    #region PROPERTIES
    public static GameManager Instance { get; private set; }
    #endregion

    #region EVENTS
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 300;
    }

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        StartGame();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene(0);
        }
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
        CameraController.Instance.MoveToBegin();
        CameraController.Instance.ChangeColor();
        // ItemManager.Instance.CheckStatus();
        Conveyor.Instance.SpawnCakes();

        DataManager.Instance.CheckKeys();

        Gate.Instance.Refresh();
        if (DataManager.Instance.currentLevel == 0)
        {
            Gate.Instance.canMove = true;
        }
        else
        {
            Gate.Instance.canMove = false;
        }
        // Debug.Log("Play: " + Gate.Instance.canMove);

    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
        CameraController.Instance.MoveToBegin();
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);

        // Gate.Instance.Refresh();
        Gate.Instance.baseSpeed = 9f + (int)(DataManager.Instance.currentLevel / 5f) * 0.6f;
        Gate.Instance.baseSpeed = Mathf.Clamp(Gate.Instance.baseSpeed, 9f, 14f);
        Gate.Instance.canMove = true;
        Gate.Instance.moveSpeed = Gate.Instance.baseSpeed;

        // Debug.Log("Play: " + Gate.Instance.baseSpeed);
        StartCoroutine(C_Analytics_Start());
    }

    private IEnumerator C_Analytics_Start()
    {
        yield return new WaitForSeconds(1.0f);
        AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.StartEvent);
    }
    private IEnumerator C_Analytics_End()
    {
        yield return new WaitForSeconds(1.0f);
        AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);
    }

    public void RestartGame()
    {
        Debug.Log("Restart!");
        ChangeState(GameState.INGAME);
        CameraController.Instance.MoveToBegin();
        CameraController.Instance.ChangeColor();
        Conveyor.Instance.SpawnCakes();
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);

        Gate.Instance.Refresh();
        Gate.Instance.canMove = true;

        Gate.Instance.baseSpeed = 9f + (int)(DataManager.Instance.currentLevel / 5f) * 0.6f;
        Gate.Instance.baseSpeed = Mathf.Clamp(Gate.Instance.baseSpeed, 9f, 14f);
        // try
        // {
        //     FastFood.Instance.ActiveFastFood(0);
        // }
        // catch{}
        // Gate.Instance.moveSpeed = Gate.Instance.baseSpeed;
    }

    private int GetWinCoins()
    {
        int total = (Shop.Instance.ingredientPrice + Shop.Instance.stackPrice + Shop.Instance.offlineEarningPrice) / 3;
        float progress = Conveyor.Instance.currentCake.IngredientCollected * 1f / Conveyor.Instance.currentCake.ingredientList.Count;
        // Debug.LogFormat("Total earnable {0}, progress {1}, earned {2}", total, progress, total * progress);
        return Mathf.RoundToInt(total * progress);
    }

    public void WinGame()
    {
        StartCoroutine(C_Analytics_End());
        ChangeState(GameState.WINGAME);
        EazySoundManager.PlaySound(SoundManager.Instance.winGame);

        if (DataManager.Instance.IsBonusLevel())
        {
            UIManager.Instance.OnWinGame();
        }
        else
        {
            if (DataManager.Instance.currentLevel == 0
                || DataManager.Instance.currentLevel == 2
                || DataManager.Instance.currentLevel % 4 == 0)
            {
                Shop.Instance.ShowComplete(
                              Conveyor.Instance.currentCake.fliedIngredientAmount,
                              /*(int)(Conveyor.Instance.currentCake.fliedIngredientAmount + DataManager.Instance.currentLevel) * 5*/
                              GetWinCoins(),
                              true, Conveyor.Instance.currentCake.isNewHeight, true);
            }
            else
            {
                Shop.Instance.ShowComplete(
                                               Conveyor.Instance.currentCake.fliedIngredientAmount,
                                               /*(int)(Conveyor.Instance.currentCake.fliedIngredientAmount + DataManager.Instance.currentLevel) * 5*/
                                               GetWinCoins(),
                                               true, Conveyor.Instance.currentCake.isNewHeight, false);

            }
        }

        // if (DataManager.Instance.currentLevel % 2 == 0 && !DataManager.Instance.IsBonusLevel())
        // {
        //     Shop.Instance.ShowComplete(
        //         Conveyor.Instance.currentCake.fliedIngredientAmount,
        //         (int)(Conveyor.Instance.currentCake.fliedIngredientAmount) * 5,
        //         true, Conveyor.Instance.currentCake.isNewHeight, true);
        // }
        // else
        // {
        //     if (DataManager.Instance.IsBonusLevel())
        //     {
        //         UIManager.Instance.OnWinGame();
        //     }
        //     else
        //     {
        //         Shop.Instance.ShowComplete(
        //                         Conveyor.Instance.currentCake.fliedIngredientAmount,
        //                         (int)(Conveyor.Instance.currentCake.fliedIngredientAmount) * 5,
        //                         true, Conveyor.Instance.currentCake.isNewHeight, false);
        //     }
        // }

        DataManager.Instance.currentLevel++;
        DataManager.Instance.SaveData();
        //UnityAdsManager.Instance.UpdateCompleteToShowVideoAds();
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
        DataManager.Instance.SaveData();
        //UnityAdsManager.Instance.UpdateDeadToShowVideoAds();

    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        // Debug.Log(currentState);
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
