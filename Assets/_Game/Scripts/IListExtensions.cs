﻿using System.Collections.Generic;
using UnityEngine;

public static class IListExtensions
{
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    /// <summary>
    /// Shuffles in Range the element order of the specified list.
    /// </summary>
    public static void ShuffleRange<T>(this IList<T> ts, int start, int end)
    {
        var count = ts.Count;
        if (end >= count)
        {
            Debug.Log("Out of Range Shuffle");
            return;
        }
        // var last = count - 1;
        for (var i = start; i < end; ++i)
        {
            var r = UnityEngine.Random.Range(i, end + 1);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}