﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Key : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public MeshRenderer meshRenderer;
    public BoxCollider box;
    public Animator animator;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static Key Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        HideKey();
    }

    public void ShowKey()
    {
        box.enabled = true;
        animator.enabled = true;
        meshRenderer.enabled = true;
        box.enabled = true;
    }

    public void HideKey()
    {
        meshRenderer.enabled = false;
        box.enabled = false;
    }

    public void SetPosition(Vector3 pos)
    {
        gameObject.SetActive(true);
        transform.position = pos;
        ShowKey();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Gate0") || other.CompareTag("Gate1"))
        {
            DataManager.Instance.ChangeKey(1);
            animator.enabled = false;
            box.enabled = false;
            transform.DOJump(new Vector3(-5f, 0f, transform.position.z + 5f), 5f, 1, 1.25f);
            transform.DORotate(new Vector3(0f, 0f, 720f), 1, RotateMode.FastBeyond360);
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            // HideKey();
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
