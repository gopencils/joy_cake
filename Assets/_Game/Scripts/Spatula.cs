﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Spatula : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Animator animator;
    public Transform spatulaObj;
    public MeshRenderer spatulaObjMeshRenderer;
    public Material spatulaNormalMaterial;
    // public Material spatulaFeverMaterial;
    #endregion

    #region PARAMS
    private bool isFlipping;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        transform.localPosition = new Vector3(0f, 0.5f, -1.4f);
        SetNormalMaterial();
    }

    [NaughtyAttributes.Button]
    public void Flick()
    {
        animator.SetTrigger("Flick");
        EazySoundManager.PlaySound(SoundManager.Instance.flick);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();
    }

    public void FlipLoop()
    {
        if (!isFlipping && gameObject.activeSelf)
        {
            StartCoroutine(C_FlipLoop());
        }
    }

    IEnumerator C_FlipLoop()
    {
        isFlipping = true;
        animator.SetTrigger("Flip");
        yield return new WaitForSeconds(0.1f);
        isFlipping = false;
    }

    [NaughtyAttributes.Button]
    public void Flip()
    {
        animator.SetTrigger("Flip");
        EazySoundManager.PlaySound(SoundManager.Instance.flip);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("FrontDish") && gameObject.activeSelf)
        {
            // Debug.Log("FrontDish");
            StartCoroutine(C_OnFrontDish());
        }
    }

    IEnumerator C_OnFrontDish()
    {
        spatulaObj.DOLocalMoveX(-1.25f, 0.3f);
        yield return new WaitForSeconds(0.3f);
        spatulaObj.DOLocalMoveX(0f, 0.3f);
        yield return new WaitForSeconds(0.5f);

        if (Conveyor.Instance.currentCake.fliedIngredientAmount > 1)
        {
            EazySoundManager.PlaySound(SoundManager.Instance.completeCake);
            yield return new WaitForSeconds(0.2f);
            EazySoundManager.PlaySound(SoundManager.Instance.completeCakeClap);

            UIManager.Instance.UpdateLevelCircle(1f);
            Conveyor.Instance.OnCompledtedLevel();
        }
        else
        {
            if (DataManager.Instance.IsBonusLevel())
            {
                Conveyor.Instance.Refresh();
                GameManager.Instance.WinGame();
            }
            else
            {
                GameManager.Instance.LoseGame();
            }
        }

    }

    [NaughtyAttributes.Button]
    public void Celebrate()
    {
        animator.SetTrigger("Celebrate");
    }

    [NaughtyAttributes.Button]
    public void SetNormalMaterial()
    {
        // Debug.Log("SetNormalMaterial");

        var mats = spatulaObjMeshRenderer.materials;
        mats[1] = spatulaNormalMaterial;
        spatulaObjMeshRenderer.materials = mats;

        spatulaObjMeshRenderer.materials[1] = spatulaNormalMaterial;
    }

    // [NaughtyAttributes.Button]
    // public void SetFeverMaterial()
    // {
    //     // Debug.Log("SetFeverMaterial");
    //     var mats = spatulaObjMeshRenderer.materials;
    //     mats[1] = spatulaFeverMaterial;
    //     spatulaObjMeshRenderer.materials = mats;

    //     spatulaObjMeshRenderer.materials[1] = spatulaFeverMaterial;
    // }

    #endregion

    #region DEBUG
    #endregion

}
