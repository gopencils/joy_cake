﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Bread : MonoBehaviour
{
    private bool isFlied;
    private bool isFlying;

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Transform target;
    public Cake parentCake;
    public MeshRenderer meshRenderer;
    public GameObject trail;

    #endregion

    #region PARAMS
    Tween jumpTween;
    Tween rotateTween;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        StopTweens();
        StopAllCoroutines();
        isFlied = false;
        isFlying = false;
        transform.rotation = Quaternion.identity;
        transform.SetParent(parentCake.transform);
        trail.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isFlying && !isFlied)
        {
            // if (Gate.Instance.isFevering)
            // {
            //     if (other.CompareTag("Gate1") || other.CompareTag("Gate0"))
            //     {
            //         JumpToDish(target.transform.position, 1, 3.5f, 1f);
            //         Gate.Instance.spatula.Flip();
            //     }
            // }
            // else
            // {
            if (other.CompareTag("Gate1"))
            {
                JumpToDish(target.transform.position, 1, 3.5f, 1f);
                Gate.Instance.spatula.Flip();
            }
            // }

        }

        // if (Gate.Instance.isFever)
        // {
        //     if ((other.CompareTag("Gate1") || other.CompareTag("Gate0"))
        //         && !isFlying && !isFlied)
        //     {
        //         JumpToDish(target.transform.position, 1, 3f, 1f);
        //         Gate.Instance.spatula.Flip();
        //     }
        // }
    }

    void JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        parentCake.currentHeight += 0.2f;
        Vector3 temp = new Vector3(targetPos.x, parentCake.currentHeight, targetPos.z);
        if (DataManager.Instance.IsBonusLevel())
        {
            trail.SetActive(false);
        }
        else
        {
            trail.SetActive(true);
        }
        StartCoroutine(C_JumpToDish(temp, turn, jumpPower, duration));
    }

    IEnumerator C_JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        isFlied = true;
        isFlying = true;
        jumpTween = transform.DOJump(targetPos, jumpPower, 1, duration, false).SetEase(Ease.Flash);
        rotateTween = transform.DORotate(new Vector3(360f * turn, 0f, 0f), duration * 0.75f, RotateMode.FastBeyond360).SetEase(Ease.Flash);
        yield return new WaitForSeconds(duration - 0.3f);

        if (DataManager.Instance.IsBonusLevel())
        {
            yield return null;
        }
        else
        {
            parentCake.ShowAura();
        }

        yield return new WaitForSeconds(0.3f);

        isFlying = false;
        transform.SetParent(target);
        parentCake.fliedIngredientAmount++;

        // Debug.Log("Cake Completed");
        // Gate.Instance.box.enabled = false;

        // EazySoundManager.PlaySound(SoundManager.Instance.completeCake);
        // yield return new WaitForSeconds(0.2f);
        // EazySoundManager.PlaySound(SoundManager.Instance.completeCakeClap);

        // parentCake.isCompleted = true;
        // parentCake.taskUI.ShowCheck();

        // Conveyor.Instance.currentCakeCompleted++;
        // Conveyor.Instance.UpdateStatus();
        yield return new WaitForSeconds(1f);
        Hide();
    }

    public void StopTweens()
    {
        jumpTween.Kill(false);
        rotateTween.Kill(false);
    }



    public void Show()
    {
        meshRenderer.enabled = true;
    }
    public void Hide()
    {
        meshRenderer.enabled = false;

    }

    #endregion

    #region DEBUG
    #endregion

}
