﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class OfflineEarning : MonoBehaviour
{
    private const string PREF_KEY_LAST_DATE = "LASTDATETIME";

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Text earnCoinText;
    #endregion

    #region PARAMS
    DateTime currentDate;
    DateTime oldDate;
    TimeSpan interval;

    public int intervalMinutes;
    public int earnCoins;

    public bool isCollected;
    #endregion

    #region PROPERTIES
    public static OfflineEarning Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        isCollected = false;
    }

    void Start()
    {

        //Store the current time when it starts
        currentDate = System.DateTime.Now;

        //Grab the old time from the player prefs as a long
        long temp = Convert.ToInt64(PlayerPrefs.GetString(PREF_KEY_LAST_DATE));

        //Convert the old time from binary to a DataTime variable
        DateTime oldDate = DateTime.FromBinary(temp);
        // print("oldDate: " + oldDate);

        //Use the Subtract method and store the result as a timespan variable
        interval = currentDate.Subtract(oldDate);
        // print("Difference: " + interval);

        CheckReward();
    }

    /// <summary>
    /// Callback sent to all game objects before the application is quit.
    /// </summary>
    void OnApplicationQuit()
    {
        PlayerPrefs.SetString(PREF_KEY_LAST_DATE, System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.Save();
    }

    void OnApplicationPause(bool isPaused)
    {
        if (isPaused)
        {
            if (isCollected)
            {
                // Debug.LogError("Paused at " + System.DateTime.Now);
                //Save the current system time as a string in the player prefs class
                PlayerPrefs.SetString(PREF_KEY_LAST_DATE, System.DateTime.Now.ToBinary().ToString());
                PlayerPrefs.Save();
            }
        }
        else
        {
            // Debug.LogError("Run at " + System.DateTime.Now);
            CheckReward();
        }

        // Debug.Log("Saving this date to prefs: " + System.DateTime.Now);
    }

    public void CheckReward()
    {
        intervalMinutes = interval.Minutes;
        earnCoins = intervalMinutes * Shop.Instance.OfflineUpgradeCoinPerMin;
        if (earnCoins > 400 && !isCollected)
        {
            int maxOfflineEarning = PlayerPrefs.GetInt("maxOfflineEarning");
            if (earnCoins > maxOfflineEarning)
            {
                earnCoins = maxOfflineEarning;
            }
            Shop.Instance.ShowOfflineEarningPopup(earnCoins);
        }
        else
        {
            Shop.Instance.HideOfflineEarningPopup();
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
