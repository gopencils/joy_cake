﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public GameObject gate;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Start()
    {
        gate.tag = "Gate1";
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            gate.tag = "Gate1";
        }

        if (Input.GetMouseButtonUp(0))
        {
            gate.tag = "Gate0";
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
