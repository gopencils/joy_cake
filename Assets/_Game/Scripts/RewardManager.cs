﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static RewardManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public void AddCoin(int addedCoin)
    {
        DataManager.Instance.currentCoin += addedCoin;
        DataManager.Instance.SaveData();
    }

    public void SubCoin(int addedCoin)
    {
        DataManager.Instance.currentCoin -= addedCoin;
        DataManager.Instance.SaveData();
    }

    #endregion

    #region DEBUG
    #endregion

}
