﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public Camera cam;
    public List<Color> colorList;
    public Image imgGradient;
    public Vector3 originLPos;
    public Vector3 originLRot;
    public float fov;


    public static CameraController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Refresh();
        UpdateFOV();
    }

    public void Refresh()
    {
        transform.SetParent(Gate.Instance.transform);
        transform.localPosition = originLPos;
        transform.localRotation = Quaternion.Euler(originLRot);
        cam.fieldOfView = 60;
    }

    public void UpdateFOV()
    {
        float screenRatio = cam.aspect;
        if (screenRatio <= 0.475f)  //9:19
        {
            cam.fieldOfView = 70f;
        }
        else
        if (screenRatio <= 0.565f)  //9:16
        {
            cam.fieldOfView = 60f;
        }
        else
        if (screenRatio <= 0.75)  //3:4
        {
            cam.fieldOfView = 55f;
        }
        fov = cam.fieldOfView;
    }

    public void LocalMove(Vector3 targetPos, Vector3 targetRot, float duration)
    {
        transform.DOLocalMove(targetPos, duration);
        transform.DOLocalRotate(targetRot, duration, RotateMode.Fast);
    }

    // public void Move(Vector3 targetPos, float duration, Ease ease = Ease.Flash)
    // {
    //     transform.DOMove(targetPos, duration).SetEase(Ease.Flash);
    // }

    public void MoveToBegin()
    {
        LocalMove(originLPos, originLRot, 1f);
    }

    public void Zoom(float fov, float duration)
    {
        StartCoroutine(C_Zoom(fov, duration));
    }

    IEnumerator C_Zoom(float fov, float duration)
    {
        float start = cam.fieldOfView;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / duration;
            cam.fieldOfView = Mathf.Lerp(start, fov, t);
            yield return null;
        }
    }

    public void ChangeColor()
    {
        Color rand = colorList[(int)Random.Range(0f, colorList.Count)];
        //cam.backgroundColor = rand;
        imgGradient.color = rand;
    }

}
