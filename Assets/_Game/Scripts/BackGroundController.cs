﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    public List<Sprite> bgList;
    public SpriteRenderer spriteRenderer;

    public static BackGroundController Instance { get; private set; }

    private int index = -1;
    private void Awake()
    {
        Instance = this;
    }

    public void ChangeBG()
    {
        index++;
        if (index >= bgList.Count)
        {
            index = 0;
        }
        spriteRenderer.sprite = bgList[index];
    }
}
