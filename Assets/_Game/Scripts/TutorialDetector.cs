﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDetector : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public bool isShownHold;
    public bool isShownRelease;


    // private void OnTriggerEnter(Collider other)
    // {
    //     if (DataManager.Instance.currentLevel == 0)
    //     {
    //         if (other.gameObject.CompareTag("Ingredient") && !isShownHold)
    //         {
    //             isShownHold = true;
    //             Gate.Instance.ChangeSpeed(0f, 0.15f);
    //             UIManager.Instance.ShowTutorialHold();
    //         }
    //         if (other.gameObject.CompareTag("Obstacle") && !isShownRelease)
    //         {
    //             isShownRelease = true;
    //             Gate.Instance.ChangeSpeed(0f, 0.15f);
    //             UIManager.Instance.ShowTutorialRelease();
    //         }
    //     }
    // }

    #endregion

    #region DEBUG
    #endregion

}
