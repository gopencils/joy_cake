﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class Gate : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public float baseSpeed;
    // public float feverSpeed;
    public Color[] moitruongcolorskyfog;
    public GameObject[] moitruong;
    public GameObject gateObj;
    public Spatula spatula;
    public ParticleSystem ps;
    Vector3 firstPos;
    Vector3 firstRot;
    // public float durationToStartFever;
    // public float feverDuration;
    // public float durationBetween2Fevers;
    // public float endFeverWarningDuration;

    // public Transform feverParticleSystem;

    #endregion

    #region PARAMS
    const float psSpeed = 1f;
    ParticleSystem.MainModule main;
    public static Gate Instance { get; private set; }
    public float moveSpeed;
    public bool canMove = true;
    Coroutine changeSpeedCoroutine;

    public bool isMissIngredient;
    // public bool isFevering;
    // public bool isFevered;
    // Coroutine checkFeverCoroutine;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        firstPos = spatula.transform.position;
        firstRot = spatula.transform.eulerAngles;
        Instance = this;
        main = ps.main;
    }

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        StopAllCoroutines();
        moveSpeed = 0f;
        canMove = false;
        gateObj.tag = "Gate0";
        transform.position = Vector3.zero;
        //spatula.transform.position = new Vector3(0, 0.5f, -1.5f);
        //spatula.transform.eulerAngles = new Vector3(0, 180, 0);
        main.simulationSpeed = psSpeed;
        isMissIngredient = false;
        StartCoroutine(delaySpawnMap());
        // try
        // {
        //     FastFood.Instance.ActiveFastFood(1);
        // }
        // catch { }
        // isFevering = false;
        // isFevered = false;
        // isCheckedFever = false;
        // feverParticleSystem.gameObject.SetActive(false);
    }

    public int currentmt;
    public void OnClick_ChangeMoiTruong()
    {
        moitruong[0].SetActive(false);
        moitruong[1].SetActive(false);
        currentmt++;
        if (currentmt == 2) currentmt = 0;
        moitruong[currentmt].SetActive(true);
        RenderSettings.fogColor = moitruongcolorskyfog[currentmt];
        Camera.main.backgroundColor = moitruongcolorskyfog[currentmt];
    }

    IEnumerator delaySpawnMap()
    {
        yield return null;
        //var mapObject = FastFood.Instance.transform.parent.gameObject;
        //mapObject.SetActive(false);
        //mapObject.SetActive(true);
        if(DataManager.Instance.currentLevel % 2 == 0)
        {
            moitruong[0].SetActive(true);
            moitruong[1].SetActive(false);
            RenderSettings.fogColor = moitruongcolorskyfog[0];
            Camera.main.backgroundColor = moitruongcolorskyfog[0];
        }
        else
        {
            moitruong[0].SetActive(false);
            moitruong[1].SetActive(true);
            RenderSettings.fogColor = moitruongcolorskyfog[1];
            Camera.main.backgroundColor = moitruongcolorskyfog[1];
        }
        yield return new WaitForSeconds(0.1f);
    }

    // public bool isCheckedFever;

    private void FixedUpdate()
    {
        if (GameManager.Instance.currentState != GameState.INGAME)
        {
            return;
        }

#if UNITY_EDITOR
        // if (!isFevering)
        // {
        if (Input.GetMouseButtonDown(0) && canMove)
        {
            gateObj.tag = "Gate1";
            moveSpeed = baseSpeed;
            // if (!isCheckedFever)
            // {
            //     if (DataManager.Instance.currentLevel > 15 && !DataManager.Instance.IsBonusLevel())
            //     {
            //         CheckFever();
            //         isCheckedFever = true;
            //     }
            // }

        }
        else
        if (Input.GetMouseButton(0) && canMove)
        {
            gateObj.tag = "Gate1";
            spatula.FlipLoop();
        }
        else
        {
            gateObj.tag = "Gate0";
        }
        // }
#elif UNITY_IOS || UNITY_ANDROID
        // if (!isFevering)
        // {
        if (Input.touchCount > 0 && canMove)
        {
            gateObj.tag = "Gate1";
            spatula.FlipLoop();
            moveSpeed = baseSpeed;

            // if (!isCheckedFever)
            // {
            //     if (DataManager.Instance.currentLevel > 5 && !DataManager.Instance.IsBonusLevel())
            //     {
            //         Gate.Instance.CheckFever();
            //         isCheckedFever = true;
            //     }
            // }
        }
        else
        {
            gateObj.tag = "Gate0";
        }
        // }
#endif

        Move();

    }

    public void Move()
    {
        if (canMove)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        }
    }

    public void ChangeSpeed(float targetSpeed, float duration)
    {
        if (changeSpeedCoroutine != null)
        {
            StopCoroutine(changeSpeedCoroutine);
        }
        changeSpeedCoroutine = StartCoroutine(C_ChangeSpeed(targetSpeed, duration));
    }

    public IEnumerator C_ChangeSpeed(float targetSpeed, float duration)
    {
        float t = 0f;
        float tempSpeed = moveSpeed;
        while (t < 1f)
        {
            t += Time.deltaTime / duration;
            moveSpeed = Mathf.Lerp(tempSpeed, targetSpeed, t);
            yield return null;
        }
    }

    public void FasterPS()
    {
        main.simulationSpeed += 0.4f;
        main.simulationSpeed = Mathf.Clamp(main.simulationSpeed, 1f, 3f);
    }

    // public void StopCheckFever()
    // {
    //     if (checkFeverCoroutine != null)
    //     {
    //         StopCoroutine(checkFeverCoroutine);
    //     }
    //     UIManager.Instance.UpdateImgFever(0f);
    // }

    // public void CheckFever()
    // {
    // Debug.Log("CheckFever");
    // if (isFevered || DataManager.Instance.currentLevel < 15)
    // {
    //     return;
    // }

    // if (DataManager.Instance.currentLevel < 10)
    // {
    //     durationToStartFever = 5f;
    //     feverDuration = 5f;
    // }
    // else
    // {
    //     durationToStartFever = 7f;
    //     feverDuration = 7f;
    // }

    // isMissIngredient = false;
    // if (checkFeverCoroutine != null)
    // {
    //     StopCheckFever();
    // }
    // checkFeverCoroutine = StartCoroutine(C_CheckFever());
    // }

    // IEnumerator C_CheckFever()
    // {
    //     float t = 0f;
    //     while (t < 1f)
    //     {
    //         t += Time.deltaTime / durationToStartFever;
    //         UIManager.Instance.UpdateImgFever(t);
    //         yield return null;
    //     }

    //     Debug.Log("Start Fever");
    //     UIManager.Instance.UpdateFevering(feverDuration);

    //     isFevering = true;
    //     isFevered = true;
    //     moveSpeed = feverSpeed;
    //     spatula.SetFeverMaterial();
    //     StartCoroutine(C_FeverPSFollowSpatula());

    //     yield return new WaitForSeconds(feverDuration - endFeverWarningDuration);

    //     ChangeSpeed(baseSpeed, endFeverWarningDuration);

    //     float t1 = 1f;
    //     while (t1 >= 0f)
    //     {
    //         t1 -= Time.deltaTime / endFeverWarningDuration;
    //         UIManager.Instance.UpdateImgFever(t1);
    //         yield return null;
    //     }

    //     Debug.Log("Stop Fever");
    //     isFevering = false;
    //     // moveSpeed = baseSpeed;
    //     spatula.SetNormalMaterial();

    //     // yield return new WaitForSeconds(durationBetween2Fevers);

    //     // CheckFever();
    // }

    // IEnumerator C_FeverPSFollowSpatula()
    // {
    //     feverParticleSystem.gameObject.SetActive(true);
    //     float t = 0f;
    //     while (t < 1f)
    //     {
    //         t += Time.deltaTime / feverDuration;
    //         feverParticleSystem.transform.position = spatula.transform.position;
    //         yield return null;
    //     }
    //     feverParticleSystem.gameObject.SetActive(false);
    // }

    #endregion

    #region DEBUG
    #endregion

}
