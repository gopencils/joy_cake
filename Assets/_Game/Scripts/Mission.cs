﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mission : MonoBehaviour
{
    public Image imgCheck;
    public Text coinText;
    public Text descriptionText;
    public Button btnColectMission;
    public Image imgCover;
    public bool isCompleted;
    public int rewardCoin;
    public bool isCollected;

    public void InitializeCoin(int rewardCoin)
    {
        this.rewardCoin = rewardCoin;
        SetCoin();
    }

    public void SetCoin()
    {
        coinText.text = rewardCoin.ToString();
    }

    public void SetActiveChecked(bool active)
    {
        imgCheck.gameObject.SetActive(active);
    }

    public virtual void Complete()
    {
    }

    public virtual void Uncomplete()
    {
    }

    public virtual void Collect()
    {
    }

}
