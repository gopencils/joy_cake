﻿using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;

public class PerfectMission : Mission
{
    public void Load()
    {
        isCompleted = PlayerPrefs.GetInt("PERFECT") == 1 ? true : false;
        isCollected = PlayerPrefs.GetInt("PERFECT_COLLECTED") == 1 ? true : false;

        SetActiveChecked(isCompleted);
        btnColectMission.interactable = (isCompleted && !isCollected);
        imgCover.enabled = isCollected;

    }

    [NaughtyAttributes.Button]
    public override void Complete()
    {
        if (isCompleted)
        {
            return;
        }
        PlayerPrefs.SetInt("PERFECT", 1);
        isCompleted = true;
        SetActiveChecked(isCompleted);
    }

    [NaughtyAttributes.Button]
    public override void Uncomplete()
    {
        PlayerPrefs.SetInt("PERFECT", 0);
        isCompleted = false;

        PlayerPrefs.SetInt("PERFECT_COLLECTED", 0);
        isCollected = false;

        SetActiveChecked(isCompleted);
    }

    public override void Collect()
    {
        if (isCompleted && !isCollected && DataManager.Instance != null)
        {
            DataManager.Instance.ChangeCoin(rewardCoin);
            isCollected = true;
            PlayerPrefs.SetInt("PERFECT_COLLECTED", 1);
            btnColectMission.interactable = false;
            imgCover.enabled = isCollected;

            UIManager.Instance.SpawnCoinPS(btnColectMission.transform);
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        }
    }

}
