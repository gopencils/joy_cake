﻿using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

public class TotalNewStackMission : Mission
{
    public Image fillBar;
    public int value;
    public int totalNewStacksAmount;

    public void InitializeValue(int value)
    {
        this.value = value;
    }

    public void Load()
    {

        totalNewStacksAmount = PlayerPrefs.GetInt("TOTALSTACKS_AMOUNT");
        isCompleted = totalNewStacksAmount >= value ? true : false;

        SetVisual();

        isCollected = PlayerPrefs.GetInt("TOTALNEWSTACKS_COLLECTED") == 1 ? true : false;
        SetActiveChecked(isCompleted);
        btnColectMission.interactable = (isCompleted && !isCollected);
        imgCover.enabled = isCollected;

    }

    public void UpdateAmount(int amount)
    {
        this.totalNewStacksAmount = amount;
        PlayerPrefs.SetInt("TOTALSTACKS_AMOUNT", totalNewStacksAmount);
        SetVisual();
    }

    public void UpdateAmount()
    {
        PlayerPrefs.SetInt("TOTALSTACKS_AMOUNT", totalNewStacksAmount);
        SetVisual();
        if (this.totalNewStacksAmount >= value)
        {
            Complete();
        }
    }

    [NaughtyAttributes.Button]
    public override void Complete()
    {
        UpdateAmount(value);
        isCompleted = true;
        SetActiveChecked(isCompleted);
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Uncomplete()
    {
        UpdateAmount(0);

        isCompleted = false;
        PlayerPrefs.SetInt("TOTALNEWSTACKS_COLLECTED", 0);
        isCollected = false;

        SetActiveChecked(isCompleted);

        SetVisual();
    }

    public void SetVisual()
    {
        descriptionText.text = "UNLOCK " + value + " STACK" + (value == 1 ? "" : "S");
        fillBar.fillAmount = (float)totalNewStacksAmount / (float)value;
    }

    public override void Collect()
    {
        if (isCompleted && !isCollected && DataManager.Instance != null)    //Collect Coin OnEnable
        {
            DataManager.Instance.ChangeCoin(rewardCoin);
            isCollected = true;
            PlayerPrefs.SetInt("TOTALNEWSTACKS_COLLECTED", 1);
            btnColectMission.interactable = false;
            imgCover.enabled = isCollected;

            UIManager.Instance.SpawnCoinPS(btnColectMission.transform);
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        }
    }
}
