﻿using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

public class HighestCakeMission : Mission
{
    public Image fillBar;
    public Text highestText;
    public int value;
    public int highestCake;

    public void InitializeValue(int value)
    {
        this.value = value;
    }

    public void Load()
    {

        highestCake = PlayerPrefs.GetInt("HIGHEST");
        isCompleted = highestCake >= value ? true : false;

        SetVisual();

        isCollected = PlayerPrefs.GetInt("HIGHESTCAKE_COLLECTED") == 1 ? true : false;
        SetActiveChecked(isCompleted);
        btnColectMission.interactable = (isCompleted && !isCollected);
        imgCover.enabled = isCollected;

    }

    public void UpdateHeight(int amount)
    {
        if (amount > value)
        {
            Complete();
        }
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Complete()
    {
        UpdateHeight(value);
        isCompleted = true;
        highestCake = value;
        SetActiveChecked(isCompleted);
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Uncomplete()
    {
        UpdateHeight(0);

        isCompleted = false;
        PlayerPrefs.SetInt("HIGHEST", 0);
        PlayerPrefs.SetInt("HIGHESTCAKE_COLLECTED", 0);
        isCollected = false;

        SetActiveChecked(isCompleted);

        SetVisual();
    }

    public void SetVisual()
    {
        descriptionText.text = "HIGHEST HAMBURGER";
        highestText.text = value.ToString();
        fillBar.fillAmount = (float)highestCake / (float)value;
    }

    public override void Collect()
    {
        if (isCompleted && !isCollected && DataManager.Instance != null)    //Collect Coin OnEnable
        {
            DataManager.Instance.ChangeCoin(rewardCoin);
            isCollected = true;
            PlayerPrefs.SetInt("HIGHESTCAKE_COLLECTED", 1);
            btnColectMission.interactable = false;
            imgCover.enabled = isCollected;
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();
        }
    }

}
