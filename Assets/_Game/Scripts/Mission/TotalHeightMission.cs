﻿using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

public class TotalHeightMission : Mission
{
    public Image fillBar;
    public Text highestText;
    public int value;
    public int totalHeight;

    public void InitializeValue(int value)
    {
        this.value = value;
    }

    public void Load()
    {

        totalHeight = PlayerPrefs.GetInt("TOTALHEIGHT");
        isCompleted = totalHeight >= value ? true : false;


        SetVisual();

        isCollected = PlayerPrefs.GetInt("TOTALHEIGHT_COLLECTED") == 1 ? true : false;
        SetActiveChecked(isCompleted);
        btnColectMission.interactable = (isCompleted && !isCollected);
        imgCover.enabled = isCollected;

    }

    public void UpdateTotalHeight()
    {
        PlayerPrefs.SetInt("TOTALHEIGHT", totalHeight);
        if (this.totalHeight > value)
        {
            Complete();
        }
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Complete()
    {
        isCompleted = true;
        totalHeight = value;
        PlayerPrefs.SetInt("TOTALHEIGHT_COLLECTED", totalHeight);
        SetActiveChecked(isCompleted);
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Uncomplete()
    {
        isCompleted = false;
        totalHeight = 0;
        PlayerPrefs.SetInt("TOTALHEIGHT", 0);
        PlayerPrefs.SetInt("TOTALHEIGHT_COLLECTED", totalHeight);
        isCollected = false;
        SetActiveChecked(isCompleted);
        SetVisual();
    }

    public void SetVisual()
    {
        descriptionText.text = "ARCHIEVE x FEET";
        highestText.text = value.ToString();
        fillBar.fillAmount = (float)totalHeight / (float)value;
        // print("percent: " + fillBar.fillAmount);
    }

    public override void Collect()
    {
        if (isCompleted && !isCollected && DataManager.Instance != null)    //Collect Coin OnEnable
        {
            DataManager.Instance.ChangeCoin(rewardCoin);
            isCollected = true;
            PlayerPrefs.SetInt("TOTALHEIGHT_COLLECTED", 1);
            btnColectMission.interactable = false;
            imgCover.enabled = isCollected;

            UIManager.Instance.SpawnCoinPS(btnColectMission.transform);
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        }
    }
}
