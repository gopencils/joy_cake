﻿using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

public class TotalNewIngredientsMission : Mission
{
    public Image fillBar;
    public int value;
    public int totalNewIngredientsAmount;

    public void InitializeValue(int value)
    {
        this.value = value;
    }

    public void Load()
    {

        totalNewIngredientsAmount = PlayerPrefs.GetInt("TOTALNEWINGREDIENTS_AMOUNT");
        isCompleted = totalNewIngredientsAmount >= value ? true : false;

        SetVisual();

        isCollected = PlayerPrefs.GetInt("TOTALNEWINGREDIENTS_COLLECTED") == 1 ? true : false;
        SetActiveChecked(isCompleted);
        btnColectMission.interactable = (isCompleted && !isCollected);
        imgCover.enabled = isCollected;

    }

    public void UpdateAmount(int amount)
    {
        this.totalNewIngredientsAmount = amount;
        PlayerPrefs.SetInt("TOTALNEWINGREDIENTS_AMOUNT", totalNewIngredientsAmount);
        SetVisual();
    }

    public void UpdateAmount()
    {
        PlayerPrefs.SetInt("TOTALNEWINGREDIENTS_AMOUNT", totalNewIngredientsAmount);
        SetVisual();
        if (this.totalNewIngredientsAmount >= value)
        {
            Complete();
        }
    }

    [NaughtyAttributes.Button]
    public override void Complete()
    {
        UpdateAmount(value);
        isCompleted = true;
        SetActiveChecked(isCompleted);
        SetVisual();
    }

    [NaughtyAttributes.Button]
    public override void Uncomplete()
    {
        UpdateAmount(0);

        isCompleted = false;
        PlayerPrefs.SetInt("TOTALNEWINGREDIENTS_COLLECTED", 0);
        isCollected = false;

        SetActiveChecked(isCompleted);

        SetVisual();
    }

    public void SetVisual()
    {
        descriptionText.text = "UNLOCK " + value + " INGREDIENT" + (value == 1 ? "" : "S");
        fillBar.fillAmount = (float)totalNewIngredientsAmount / (float)value;
    }

    public override void Collect()
    {
        if (isCompleted && !isCollected && DataManager.Instance != null)    //Collect Coin OnEnable
        {
            DataManager.Instance.ChangeCoin(rewardCoin);
            isCollected = true;
            PlayerPrefs.SetInt("TOTALNEWINGREDIENTS_COLLECTED", 1);
            btnColectMission.interactable = false;
            imgCover.enabled = isCollected;

            UIManager.Instance.SpawnCoinPS(btnColectMission.transform);
            EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        }
    }

}
