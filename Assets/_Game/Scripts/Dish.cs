﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dish : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Cake parentCake;
    public MeshRenderer meshRenderer;
    public GameObject botBread;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS
    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.CompareTag("Bound") && !parentCake.isCompleted)
    //     {
    //         if (parentCake.isLastCake)
    //         {
    //             parentCake.transform.position =
    //             new Vector3(0f, 0f, Gate.Instance.transform.position.z + 20f);
    //         }
    //         else
    //         {
    //             parentCake.transform.position =
    //             new Vector3(0f, 0f, Conveyor.Instance.lastCake.dish.transform.position.z + 10f);
    //             Conveyor.Instance.lastCake.isLastCake = false;
    //             Conveyor.Instance.lastCake = parentCake;
    //             parentCake.isLastCake = true;
    //         }
    //     }
    // }

    public void Hide()
    {
        meshRenderer.enabled = false;
        botBread.SetActive(false);
    }

    public void Show()
    {
        meshRenderer.enabled = true;
        botBread.SetActive(true);
    }

    #endregion

    #region DEBUG
    #endregion

}
