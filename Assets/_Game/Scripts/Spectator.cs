﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectator : MonoBehaviour
{
    private Rigidbody rigid;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        transform.rotation = Quaternion.LookRotation(transform.parent.position - transform.position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        float force = Random.Range(250, 370);
        rigid.velocity = Vector3.zero;
        rigid.AddForce(force * Vector3.up);
    }
}
