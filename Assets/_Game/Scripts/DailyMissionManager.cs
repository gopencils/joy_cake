﻿using System.Collections.Generic;
using System.Linq;
using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct DailyMissionData
{
    public int totalNewIngredient;
    public int totalNewStack;
    public int highestCake;
    public int totalHeight;
}

[System.Serializable]
public struct DailyMissionCoin
{
    public int coin0;
    public int coin1;
    public int coin2;
    public int coin3;
    public int coin4;
    public int coin5;
    public int coin6;
}

public class DailyMissionManager : MonoBehaviour
{

    #region CONST
    public const string NEW_INGREDIENT_KEY = "NEW_INGREDIENT_KEY";
    public const string TOTAL_NEW_INGREDIENT_KEY = "FOUR_INGREDIENT_KEY";
    public const string HIGHEST_CAKE_KEY = "HIGHEST_CAKE_KEY";
    public const string TOTAL_HEIGHT_KEY = "TOTAL_HEIGHT_KEY";
    public const string TOTAL_NEW_STACK_KEY = "TOTAL_STACK_KEY";
    public const string PERFECT_FLICK_KEY = "PERFECT_FLICK_KEY";
    public const string NEW_STACK_KEY = "NEW_STACK_KEY";
    #endregion

    #region EDITOR PARAMS

    public List<DailyMissionData> dailyMissionsList = new List<DailyMissionData>();
    public DailyMissionData currentDailyMissionData;

    public List<DailyMissionCoin> dailyMissionsCoinList = new List<DailyMissionCoin>();
    public DailyMissionCoin currentDailyMissionCoin;

    [Header("UI Daily Missions")]
    public List<GameObject> ticksList;
    public Button btnReward;
    public Button btnMission;
    public Text rewardMissionText;

    [Header("Missions")]
    public List<Mission> missionsList;
    [Space(20)]
    public NewIngredientMission newIngredientMission;
    public TotalNewIngredientsMission totalNewIngredientsMission;
    public NewStackMission newStackMission;
    public TotalNewStackMission totalNewStackMission;
    public HighestCakeMission highestCakeMission;
    public TotalHeightMission totalHeightMission;
    public PerfectMission perfectMission;

    #endregion

    #region PARAMS
    [Space(20)]

    public int missionsCompletedAmount;
    public int dailyOrder;
    #endregion

    #region PROPERTIES

    public static DailyMissionManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        Load();
    }

    public void Load()
    {
        dailyOrder = PlayerPrefs.GetInt("dailyOrder");
        GetCurrentMission();
        InitializeMissionsCoin();
        InitializeMissionData();
        CheckMissionsStatus();
        MissionsLoad();
    }

    void MissionsLoad()
    {
        newIngredientMission.Load();
        totalNewIngredientsMission.Load();
        newStackMission.Load();
        totalNewStackMission.Load();
        highestCakeMission.Load();
        totalHeightMission.Load();
        perfectMission.Load();
    }


    public void GetCurrentMission()
    {
        if (dailyOrder > 3)
        {
            dailyOrder = 3;
        }
        currentDailyMissionData = dailyMissionsList[dailyOrder];
        dailyOrder = 0;
        currentDailyMissionCoin = dailyMissionsCoinList[dailyOrder];
    }

    public void InitializeMissionsCoin()
    {
        int level = PlayerPrefs.GetInt("LEVEL");
        int bonusValue = (level + 1) / 10;
        if(bonusValue < 1)
        {
            bonusValue = 1;
        }
        rewardMissionText.text = "REWARD " + (500 * bonusValue);
        newIngredientMission.InitializeCoin(currentDailyMissionCoin.coin0 * bonusValue);
        totalNewIngredientsMission.InitializeCoin(currentDailyMissionCoin.coin1 * bonusValue);
        newStackMission.InitializeCoin(currentDailyMissionCoin.coin2 * bonusValue);
        totalNewStackMission.InitializeCoin(currentDailyMissionCoin.coin3 * bonusValue);
        highestCakeMission.InitializeCoin(currentDailyMissionCoin.coin4 * bonusValue);
        totalHeightMission.InitializeCoin(currentDailyMissionCoin.coin5 * bonusValue);
        perfectMission.InitializeCoin(currentDailyMissionCoin.coin6 * bonusValue);
    }

    public void InitializeMissionData()
    {
        totalNewIngredientsMission.InitializeValue(currentDailyMissionData.totalNewIngredient);
        totalNewStackMission.InitializeValue(currentDailyMissionData.totalNewStack);
        highestCakeMission.InitializeValue(currentDailyMissionData.highestCake);
        totalHeightMission.InitializeValue(currentDailyMissionData.totalHeight);
    }

    [NaughtyAttributes.Button]
    public void CheckMissionsStatus()
    {
        missionsCompletedAmount = 0;
        for (int i = 0; i < missionsList.Count; i++)
        {
            if (missionsList[i].isCompleted)
            {
                missionsCompletedAmount++;
            }
        }

        for (int i = 0; i < ticksList.Count; i++)
        {
            if (i < missionsCompletedAmount)
            {
                ticksList[i].SetActive(true);
            }
            else
            {
                ticksList[i].SetActive(false);
            }
        }

        if (missionsCompletedAmount >= 7)
        {
            btnReward.interactable = true;
        }
        else
        {
            btnReward.interactable = false;
        }
    }

    public void OnClick_BtnReward()
    {
        UIManager.Instance.SpawnCoinPS(btnReward.transform);
        EazySoundManager.PlaySound(SoundManager.Instance.rewardClaimed);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        int level = PlayerPrefs.GetInt("LEVEL");
        int bonusValue = (level + 1) / 10;
        if (bonusValue < 1)
        {
            bonusValue = 1;
        }
        DataManager.Instance.ChangeCoin(500 * bonusValue);
        btnReward.interactable = false;
        missionsCompletedAmount = 0;

        for (int i = 0; i < missionsList.Count; i++)
        {
            missionsList[i].Uncomplete();
        }

        dailyOrder++;
        PlayerPrefs.SetInt("dailyOrder", dailyOrder);
        Load();
        CheckMissionsStatus();
    }

    [NaughtyAttributes.Button]
    public void CompleteDaily()
    {
        missionsCompletedAmount = 7;

        for (int i = 0; i < missionsList.Count; i++)
        {
            missionsList[i].Complete();
        }
        CheckMissionsStatus();
    }

    [NaughtyAttributes.Button]
    public void Reset()
    {
        dailyOrder = 0;
        PlayerPrefs.SetInt("dailyOrder", dailyOrder);
    }

    #endregion

    #region DEBUG
    #endregion

}
