﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

[System.Serializable]
public class IngredientVisual
{
    public int id;
    public Mesh mesh;
    public Texture texture;
}

public class Ingredient : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public int id;
    public int value;

    public Rigidbody rgbd;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public List<IngredientVisual> ingredientVisualList;

    [Space(20)]
    public GameObject coin;
    public GameObject newFlag;
    #endregion

    #region PARAMS
    public Transform target;
    public bool isFlying;
    public bool isFlied;
    public Cake parentCake;
    public IngredientVisual currentIngredientVisual;

    Tween jumpTween;
    Tween rotateTween;
    private MaterialPropertyBlock propertyBlock;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnEnable()
    {
        Refresh();
    }

    public void Initialize(Transform target, int id, int value)
    {
        this.target = target;
        this.value = value;
        this.id = id;
    }

    void Refresh()
    {
        HideCoin();
        StopTweens();
        StopAllCoroutines();
        isFlying = false;
        isFlied = false;
        StopPhysics();
    }

    public void SetVisual(int id)
    {
        meshRenderer.enabled = true;

        for (int i = 0; i < ingredientVisualList.Count; i++)
        {
            if (ingredientVisualList[i].id == id)
            {
                currentIngredientVisual = ingredientVisualList[i];
            }
        }
        meshFilter.mesh = currentIngredientVisual.mesh;

        meshRenderer.GetPropertyBlock(propertyBlock);
        propertyBlock.SetTexture("_MainTex", currentIngredientVisual.texture);
        meshRenderer.SetPropertyBlock(propertyBlock);
    }

    public void HideVisual()
    {
        meshRenderer.enabled = false;
    }

    public void ShowVisual()
    {
        meshRenderer.enabled = true;
    }

    void JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        HideNewFlag();

        isFlied = true;
        parentCake.currentHeight += 0.2f;
        // parentCake.high = (int)(parentCake.currentHeight / 0.2f);
        Vector3 temp;
        if (DataManager.Instance.IsBonusLevel())
        {
            temp = new Vector3(targetPos.x, parentCake.currentHeight - 0.2f, targetPos.z);
            Conveyor.Instance.collectedCoins++;
        }
        else
        {
            temp = new Vector3(targetPos.x, parentCake.currentHeight, targetPos.z);
            Conveyor.Instance.collectedCoins = 0;
            parentCake.fliedIngredientAmount++;
            parentCake.fliedIngredientsList.Add(this);
        }
        StartCoroutine(C_JumpToDish(temp, turn, jumpPower, duration));
    }

    IEnumerator C_JumpToDish(Vector3 targetPos, int turn, float jumpPower, float duration)
    {
        transform.SetParent(target);
        isFlying = true;
        jumpTween = transform.DOJump(targetPos, jumpPower, 1, duration, false).SetEase(Ease.Flash);
        rotateTween = transform.DORotate(new Vector3(360f * turn, 0, 0f), duration, RotateMode.FastBeyond360).SetEase(Ease.Flash);
        yield return new WaitForSeconds(duration);
        isFlying = false;
        yield return new WaitForEndOfFrame();
        JumpToDishCompleted();
    }

    void JumpToDishCompleted()
    {
        StopPhysics();
    }

    public void StopPhysics()
    {
        rgbd.isKinematic = true;
        rgbd.useGravity = false;
        transform.rotation = Quaternion.identity;
        transform.localPosition = new Vector3(0f, transform.localPosition.y, 0f);
    }

    public void EnablePhysics()
    {
        rgbd.isKinematic = false;
        rgbd.useGravity = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        float duration;
        float jumpPower;
        //if (DataManager.Instance.currentLevel < 10)
        //{
        //    duration = 1f;
        //    jumpPower = 3f;
        //}
        //else
        //{
        //    duration = 2.5f;
        //    jumpPower = 5f;
        //}

        duration = 1.35f;
        jumpPower = 3.75f;

        if (!isFlying && !isFlied)
        {
            // if (Gate.Instance.isFevering)
            // {
            //     if (other.CompareTag("Gate1") || other.CompareTag("Gate0"))
            //     {
            //         JumpToDish(target.transform.position, 1, jumpPower, duration);
            //         Gate.Instance.spatula.Flip();
            //     }
            // }
            // else
            // {
            if (other.CompareTag("Gate1"))
            {
                JumpToDish(target.transform.position, 1, jumpPower, duration);
                Gate.Instance.spatula.Flip();
            }
            if (other.CompareTag("Gate0"))
            {
                if (!Gate.Instance.isMissIngredient && DataManager.Instance.currentLevel > 5f)
                {
                    // Debug.Log("Miss");
                    Gate.Instance.isMissIngredient = true;
                    // Gate.Instance.StopCheckFever();
                    // StartCoroutine(C_RecheckFever());
                    parentCake.isPerfect = false;
                }
            }
            // }
        }
    }

    // IEnumerator C_RecheckFever()
    // {
    //     yield return new WaitForSeconds(1f);
    //     Gate.Instance.CheckFever();
    // }

    public void StopTweens()
    {
        jumpTween.Kill(false);
        rotateTween.Kill(false);
    }

    public void HideCoin()
    {
        coin.SetActive(false);
    }

    public void ShowCoin()
    {
        coin.SetActive(true);
    }

    public void ShowNewFlag()
    {
        // Debug.Log("ShowNewFlag", gameObject);
        newFlag.SetActive(true);
        newFlag.transform.localRotation = Quaternion.Euler(0f, 180f, 25f);
        // newFlag.transform.localPosition = new Vector3(0.2f, 0.2f, 0f);
    }

    public void HideNewFlag()
    {
        newFlag.SetActive(false);
    }

    #endregion

    #region DEBUG
    #endregion

}
