﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockIngredientComplete : MonoBehaviour
{
    public Text nameText;
    public Image iconImg;

    public void OnAwake(string _name, Sprite _icon)
    {
        nameText.text = "YOU JUST UNLOCKED THE " + _name + "!";
        iconImg.sprite = _icon;
        gameObject.SetActive(true);
        Invoke("HideObject", 2.0f);
    }

    private void HideObject()
    {
        gameObject.SetActive(false);
    }
}
