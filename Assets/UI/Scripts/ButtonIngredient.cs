﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIngredient : MonoBehaviour
{
    public Image iconIngredientImg;

    public void UpdateButtonIngredient(Sprite _icon)
    {
        iconIngredientImg.sprite = _icon;
    }
}
