﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DanielLochner.Assets.SimpleScrollSnap;
using Hellmade.Sound;

[System.Serializable]
public class Stack
{
    public int ID;
    public Sprite mySprite;

    public int StackNumber
    {
        get
        {
            int n = PlayerPrefs.GetInt("stack" + ID);
            if (n == 0) PlayerPrefs.SetInt("stack" + ID, 1);

            return PlayerPrefs.GetInt("stack" + ID);
        }
        set
        {
            int _valueFixed = Mathf.Clamp(value, 1, 999);
            PlayerPrefs.SetInt("stack" + ID, _valueFixed);
        }
    }
}

public class Shop : MonoBehaviour
{
    public const string INGREDIENT_PRICE = "INGREDIENT_PRICE";
    public const string STACK_PRICE = "STACK_PRICE";
    public const string OFFLINE_EARNING_PRICE = "OFFLINE_EARNING_PRICE";

    public static Shop Instance;

    public List<Stack> listStack = new List<Stack>();

    public int MaxStackNumber
    {
        get
        {
            return PlayerPrefs.GetInt("MaxNumberStack");
        }
        set
        {
            PlayerPrefs.SetInt("MaxNumberStack", value);
        }
    }

    public int LastIngredient
    {
        get
        {
            return PlayerPrefs.GetInt("last_ingredient");
        }
        set
        {
            PlayerPrefs.SetInt("last_ingredient", value);
        }
    }

    [Header("Input Data")]
    public List<IngredientTableObject> listData = new List<IngredientTableObject>();

    [Header("References")]
    public bool isUnlock;
    public CompleteUI completeUI;
    public Wheel wheelScript;
    public UnlockIngredient unlockIngredientScript;
    public UnlockIngredientComplete unlockIngredientCompleteScript;
    public GameObject popup3keys;
    public GameObject dailyQuestOject;
    public Animator mainAnimator;
    public Animator btnIngredientAnimator;
    public Animator btnStackAnimator;
    public Animator btnOfflineAnimator;
    public ButtonIngredient buttonIngredient;

    public GameObject descriptionCoinObject;
    public Text priceIngredientText;
    public GameObject descriptionKeyObject;
    public Text priceStackText;
    public Text priceOfflineEarningText;

    public GameObject normalAnimation;
    public GameObject chestAnimation;
    public GameObject ingredientIconAnimation;

    public Text levelIngredientText;
    public Text wheelTimerText;
    public GameObject wheelNotification;
    public Image stackIcon;
    public Text stackNumberText;
    public Text stackPriceText;
    public GameObject offlineEarningPopup;
    public Text offlineEarningPopupCoinText;
    public Text offlineUpgradeCoinPerMinText;

    public Image imgGrayscaleIngredient;
    public Image imgGrayscaleStack;
    public Image imgGrayscaleOfflineEarning;

    public Text noteText;

    public int ingredientPrice;
    public int stackPrice;
    public int offlineEarningPrice;
    public int maxOfflineEarning;
    public int levelOfflineEarning;
    public int levelUpgradeStack;

    public Image imgNotiMission;
    public Button btnCollectOfflineEarningCoins;
    public Button btnPlay;

    public int OfflineUpgradeCoinPerMin
    {
        get
        {
            return PlayerPrefs.GetInt("OfflineUpgradeCoinPerMin");
        }
        set
        {
            PlayerPrefs.SetInt("OfflineUpgradeCoinPerMin", value);
            offlineUpgradeCoinPerMinText.text = value.ToString();
        }
    }

    public void OnClick_UpgradeOffline()
    {
        btnOfflineAnimator.SetTrigger("Bubble");
        if (DataManager.Instance.currentCoin < offlineEarningPrice)
        {
            return;
        }
        else
        {
            DataManager.Instance.ChangeCoin(-offlineEarningPrice);

            OfflineUpgradeCoinPerMin += 20;

            EazySoundManager.PlaySound(SoundManager.Instance.upgradeClicked);

            float multiple;
            if (DataManager.Instance.currentLevel < 12)
            {
                multiple = 1.2f;
            }
            else
            {
                multiple = 1.2f;
            }


            offlineEarningPrice = Mathf.RoundToInt((float)offlineEarningPrice * multiple);
            offlineEarningPrice = Mathf.CeilToInt(offlineEarningPrice / 10f) * 10;

            PlayerPrefs.SetInt(OFFLINE_EARNING_PRICE, offlineEarningPrice);
            MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

            levelOfflineEarning = PlayerPrefs.GetInt("levelOfflineEarning");
            levelOfflineEarning++;
            maxOfflineEarning = levelOfflineEarning * 5000;
            PlayerPrefs.SetInt("levelOfflineEarning", levelOfflineEarning);
            PlayerPrefs.SetInt("maxOfflineEarning", maxOfflineEarning);
        }
        UpdatePrice();
    }

    public void ShowOfflineEarningPopup(int coin)
    {
        offlineEarningPopup.SetActive(true);
        offlineEarningPopupCoinText.text = coin.ToString();
    }

    public void HideOfflineEarningPopup()
    {
        offlineEarningPopup.SetActive(false);
        offlineEarningPopupCoinText.text = "";
    }

    public void OnClick_CollectOfflineEarningCoins()
    {
        UIManager.Instance.SpawnCoinPS();
        maxOfflineEarning = PlayerPrefs.GetInt("maxOfflineEarning");
        if (OfflineEarning.Instance.earnCoins > maxOfflineEarning)
        {
            OfflineEarning.Instance.earnCoins = maxOfflineEarning;
        }
        DataManager.Instance.ChangeCoin(OfflineEarning.Instance.earnCoins);
        OfflineEarning.Instance.earnCoins = 0;
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
        HideOfflineEarningPopup();
        OfflineEarning.Instance.isCollected = true;
        //PlayerPrefs.SetInt(OFFLINE_EARNING_PRICE, 100);
        UpdatePrice();
        //OfflineUpgradeCoinPerMin = 10;
    }

    public void SetIngredient(int number)
    {
        PlayerPrefs.SetInt("ingredient" + number, 1);
    }

    public int GetIngredientNumber(int number)
    {
        //Debug.Log("Ingre " + PlayerPrefs.GetInt("ingredient" + number) + " id: " + number);
        return PlayerPrefs.GetInt("ingredient" + number);
    }

    private string TargetWheelTimer
    {
        get
        {
            return PlayerPrefs.GetString("TargetWheelTimer");
        }
        set
        {
            PlayerPrefs.SetString("TargetWheelTimer", value);
        }
    }

    private void Awake()
    {
        if (Instance == null) Instance = this;

        if (PlayerPrefs.GetInt("FirstTimePlay") == 0)
        {
            MaxStackNumber = 2;
            UnlockIngredientBase();
            ResetWheelTimer(0);
            PlayerPrefs.SetInt("FirstTimePlay", 1);
            LastIngredient = 0;
            OfflineUpgradeCoinPerMin = 10;

            PlayerPrefs.SetString("LASTDATETIME", System.DateTime.Now.ToBinary().ToString());
            PlayerPrefs.SetInt(INGREDIENT_PRICE, 100);
            PlayerPrefs.SetInt(STACK_PRICE, 100);
            PlayerPrefs.SetInt(OFFLINE_EARNING_PRICE, 100);
        }

        levelIngredientText.text = "LEVEL " + (DataManager.Instance.currentLevel + 1).ToString();

        GenerateStackData();
        UpgradeStackIcon();
        UpdateIngredientButton();
        ShowMainUI();
        OfflineUpgradeCoinPerMin += 0;
        btnPlay.interactable = true;
    }

    private void Start()
    {
        UpdatePrice();
    }

    [NaughtyAttributes.Button]
    public void UpdatePrice()
    {
        ingredientPrice = PlayerPrefs.GetInt(INGREDIENT_PRICE);

        priceIngredientText.text = Ultility.FormatNumber(ingredientPrice);


        stackPrice = PlayerPrefs.GetInt(STACK_PRICE);

        priceStackText.text = Ultility.FormatNumber(stackPrice);


        offlineEarningPrice = PlayerPrefs.GetInt(OFFLINE_EARNING_PRICE);

        priceOfflineEarningText.text = Ultility.FormatNumber(offlineEarningPrice);


        CheckAvailable();
    }

    void CheckAvailable()
    {
        int currentCoin = DataManager.Instance.currentCoin;
        if (currentCoin >= ingredientPrice)
        {
            imgGrayscaleIngredient.enabled = false;
        }
        else
        {
            imgGrayscaleIngredient.enabled = true;
        }

        if (currentCoin >= stackPrice)
        {
            imgGrayscaleStack.enabled = false;
        }
        else
        {
            imgGrayscaleStack.enabled = true;
        }

        if (currentCoin >= offlineEarningPrice)
        {
            imgGrayscaleOfflineEarning.enabled = false;
        }
        else
        {
            imgGrayscaleOfflineEarning.enabled = true;
        }

    }

    private void Update()
    {
        TimerWheelControl();
        UpdateButtonUnlockIngredient();
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.D)) PlayerPrefs.DeleteAll();
        if (Input.GetKeyDown(KeyCode.S)) GetIngredientData();
        if (Input.GetKeyDown(KeyCode.L)) Debug.Log("Last Ingredient (NEW)   " + LastIngredient);
        if (Input.GetKeyDown(KeyCode.R)) ShowComplete(200, 500, false, true, true);
        if (Input.GetKeyDown(KeyCode.P)) ShowPopup3Keys();
#endif
    }

    private void GenerateStackData()
    {
        for (int i = 0; i < listData.Count; i++)
        {
            Stack _stack = new Stack();
            _stack.ID = i;
            _stack.mySprite = listData[i].iconIngredient;
            if (GetIngredientNumber(_stack.ID) == 1)
            {
                listStack.Add(_stack);
            }
        }
    }

    public void AddStack(int _ID)
    {
        Stack _stack = new Stack();
        _stack.ID = _ID;
        _stack.mySprite = listData[_ID].iconIngredient;
        listStack.Add(_stack);
    }

    private void UpdateButtonUnlockIngredient()
    {
        if (DataManager.Instance.currentKey >= 3)
        {
            descriptionCoinObject.SetActive(false);
            descriptionKeyObject.SetActive(true);
        }
        else
        {
            descriptionCoinObject.SetActive(true);
            descriptionKeyObject.SetActive(false);
        }
    }

    public void OnClick_UpgradeStack()
    {
        btnStackAnimator.SetTrigger("Bubble");

        if (isUnlock) return;

        int price = stackPrice;
        if (DataManager.Instance.currentCoin < price) return;
        DataManager.Instance.ChangeCoin(-price);
        Stack _stack = GetCurrentStack();
        _stack.StackNumber++;
        UpgradeStackIcon();
        Conveyor.Instance.SpawnCakes();
        EazySoundManager.PlaySound(SoundManager.Instance.upgradeClicked);

        DailyMissionManager.Instance.newStackMission.Complete();
        DailyMissionManager.Instance.totalNewStackMission.totalNewStacksAmount++;
        DailyMissionManager.Instance.totalNewStackMission.UpdateAmount();
        CheckMissions();

        float multiple;
        if (DataManager.Instance.currentLevel < 12)
        {
            multiple = 1.1f;
        }
        else
        {
            multiple = 1.1f;
        }


        stackPrice = Mathf.RoundToInt((float)stackPrice * multiple);
        stackPrice = Mathf.CeilToInt(stackPrice / 10f) * 10;

        PlayerPrefs.SetInt(STACK_PRICE, stackPrice);
        UpdatePrice();
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();
    }

    private void UpgradeStackIcon()
    {
        Stack _stack = GetCurrentStack();
        stackIcon.sprite = _stack.mySprite;
        stackNumberText.text = (_stack.StackNumber + 1) + " PIECES";
    }

    private Stack GetCurrentStack()
    {
        for (int i = 0; i < listStack.Count; i++)
        {
            if (GetIngredientNumber(listStack[i].ID) == 1)
            {
                if (listStack[i].StackNumber < MaxStackNumber)
                {
                    return listStack[i];
                }
            }
        }

        MaxStackNumber += 2;

        return listStack[0];
    }

    private void UpdateIngredientButton()
    {
        buttonIngredient.UpdateButtonIngredient(listData[LastIngredient].iconIngredient);
    }

    public void ResetWheelTimer(int h)
    {
        System.DateTime timeNow = System.DateTime.Now;
        timeNow = timeNow.AddHours(h);
        TargetWheelTimer = timeNow.ToBinary().ToString();
    }

    private void TimerWheelControl()
    {
        string targetWheelTimer = TargetWheelTimer;
        long temp = System.Convert.ToInt64(targetWheelTimer);
        System.DateTime targetTime = System.DateTime.FromBinary(temp);
        System.TimeSpan difference = targetTime - System.DateTime.Now;

        if (difference.Hours < 0 || difference.Minutes < 0 || difference.Seconds < 0)
        {
            wheelTimerText.text = "00:00:00";

            if (wheelScript.SpinNumber <= 0) wheelScript.SpinNumber += 1;
        }
        else
        {
            wheelTimerText.text = difference.Hours + ":" + difference.Minutes + ":" + difference.Seconds;
        }

        if (wheelScript.SpinNumber > 0)
        {
            wheelNotification.SetActive(true);
        }
        else
        {
            wheelNotification.SetActive(false);
        }
    }

    private void UnlockIngredientBase()
    {
        for (int i = 0; i < 5; i++)
        {
            SetIngredient(i);
        }
    }

    public void OnClick_UnlockCoin()
    {

        if (isUnlock) return;

        // btnBuyCoinAnimator.SetTrigger("Bubble");
        btnIngredientAnimator.SetTrigger("Bubble");

        // check xem da unlock het chua , neu unlock het roi return
        if (CanUnlock() == false) return;


        // kiem tra coin , neu ko du coin return
        int myCoin = DataManager.Instance.currentCoin;
        int price = ingredientPrice;

        if (myCoin < price) return;
        DataManager.Instance.ChangeCoin(-price);

        UnlockIngredient(false);
        EazySoundManager.PlaySound(SoundManager.Instance.upgradeClicked);

        float multiple;
        if (DataManager.Instance.currentLevel < 12)
        {
            multiple = 1.4f;
        }
        else
        {
            multiple = 1.4f;
        }

        ingredientPrice = Mathf.RoundToInt((float)ingredientPrice * multiple);
        ingredientPrice = Mathf.CeilToInt(ingredientPrice / 10f) * 10;

        PlayerPrefs.SetInt(INGREDIENT_PRICE, ingredientPrice);
        UpdatePrice();
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

    }

    public void OnClick_UnlockKey()
    {

        if (isUnlock) return;
        btnIngredientAnimator.SetTrigger("Bubble");

        // check xem da unlock het chua , neu unlock het roi return
        if (CanUnlock() == false) return;

        // kiem tra coin , neu ko du coin return
        int price = 3;
        int myKey = DataManager.Instance.currentKey;


        if (myKey < price) return;

        Debug.Log("unlock random ingredient _ KEY");
        // myKey -= price;
        DataManager.Instance.currentKey = 0;
        DataManager.Instance.SaveData();

        UnlockIngredient(true);
        EazySoundManager.PlaySound(SoundManager.Instance.upgradeClicked);
        UIManager.Instance.UpdateKeys(0);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

    }

    private bool CanUnlock()
    {
        int n = 0;

        for (int i = 0; i < listData.Count; i++)
        {
            if (GetIngredientNumber(i) == 1)
            {
                n++;
            }
        }

        if (n == listData.Count)
        {
            Debug.Log("ban da unlock het ingredient");
            return false;
        }

        return true;
    }

    public void UnlockIngredientWhenComplete()
    {
        // EazySoundManager.PlaySound(SoundManager.Instance.unlockIngredient);

        if (CanUnlock() == false) return;

        // random value
        int randomValue = 0;
        int numberLoop = 0;

        for (int i = 0; i < 1; i++)
        {
            randomValue = Random.Range(0, listData.Count);

            if (GetIngredientNumber(randomValue) == 1)
            {
                randomValue = Random.Range(0, listData.Count);
                i--;

                numberLoop++;
                if (numberLoop >= 100)
                {
                    Debug.Log("Return");
                    return;
                }
            }
        }

        Debug.Log("ingredient  " + randomValue + "  UNLOCKED");

        LastIngredient = randomValue;

        //Next Unlock Level
        int temp = DataManager.Instance.currentLevel < 2 ? 2 : 4;
        noteText.text = "NEXT UNLOCK IN " + temp.ToString() + " LEVELS";

        // unlock
        SetIngredient(randomValue);
        UpdateIngredientButton();

        // chay animation (popup)
        unlockIngredientCompleteScript.OnAwake(listData[randomValue].nameIngredient, listData[randomValue].iconIngredient);

        AddStack(randomValue);

        DailyMissionManager.Instance.newIngredientMission.Complete();
        DailyMissionManager.Instance.totalNewIngredientsMission.totalNewIngredientsAmount++;
        DailyMissionManager.Instance.totalNewIngredientsMission.UpdateAmount();

    }

    public void UnlockIngredientWheel(int _ID)
    {
        LastIngredient = _ID;

        // unlock
        SetIngredient(_ID);
        UpdateIngredientButton();
        AddStack(_ID);

        // chay animation (popup)
        //  unlockIngredientCompleteScript.OnAwake(listIngredient[_ID].nameText.text, listIngredient[_ID].iconImg.sprite);

        DailyMissionManager.Instance.newIngredientMission.Complete();
        DailyMissionManager.Instance.totalNewIngredientsMission.totalNewIngredientsAmount++;
        DailyMissionManager.Instance.totalNewIngredientsMission.UpdateAmount();
    }

    public void UnlockIngredient(bool isChest)
    {
        btnPlay.interactable = false;
        EazySoundManager.PlaySound(SoundManager.Instance.upgradeClicked);
        // EazySoundManager.PlaySound(SoundManager.Instance.unlockIngredient);

        isUnlock = true;

        // random value
        int randomValue = 0;
        int numberLoop = 0;

        for (int i = 0; i < 1; i++)
        {
            randomValue = Random.Range(0, listData.Count);

            if (GetIngredientNumber(randomValue) == 1)
            {
                randomValue = Random.Range(0, listData.Count);
                i--;

                numberLoop++;
                if (numberLoop >= 200) return;
            }
        }

        Debug.Log("ingredient  " + randomValue + "  UNLOCKED");

        if (isChest)
        {
            ingredientIconAnimation.gameObject.SetActive(false);
            chestAnimation.gameObject.SetActive(true);
        }
        else
        {
            ingredientIconAnimation.gameObject.SetActive(false);
            normalAnimation.gameObject.SetActive(true);
        }


        // unlock
        LastIngredient = randomValue;
        SetIngredient(randomValue);
        UpdateIngredientButton();
        AddStack(randomValue);

        // chay animation
        StartCoroutine(C_UnlockCoinAnimation(listData[randomValue].nameIngredient, listData[randomValue].iconIngredient, isChest));

        DailyMissionManager.Instance.newIngredientMission.Complete();
        DailyMissionManager.Instance.totalNewIngredientsMission.totalNewIngredientsAmount++;
        DailyMissionManager.Instance.totalNewIngredientsMission.UpdateAmount();
        CheckMissions();
    }

    private IEnumerator C_UnlockCoinAnimation(string _name, Sprite _icon, bool _isChest)
    {
        yield return new WaitForSeconds(1.75f);

        if (_isChest)
        {
            //  chestAnimation.gameObject.SetActive(false);
        }
        else
        {
            normalAnimation.gameObject.SetActive(false);
        }
        unlockIngredientScript.OnAwake(_name, _icon);
    }

    public void ShowIngredientIconAnimationWhenClickOK()
    {
        ingredientIconAnimation.gameObject.SetActive(true);
        ingredientIconAnimation.GetComponent<Animator>().SetTrigger("Active");
        Invoke("UpdatePrice", 0.2f);
    }

    public void OnClick_CollectOffline()
    {
        if (isUnlock) return;

        btnOfflineAnimator.SetTrigger("Bubble");
        DataManager.Instance.ChangeCoin(OfflineEarning.Instance.earnCoins);
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
        EazySoundManager.PlaySound(SoundManager.Instance.rewardClaimed);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();

        OfflineEarning.Instance.isCollected = true;

        UpdatePrice();
        CheckMissions();
    }

    public void GetIngredientData()
    {
        for (int i = 0; i < listStack.Count; i++)
        {
            int ID = listStack[i].ID;
            int stankNunber = listStack[i].StackNumber;
            Debug.Log("Ingredient(" + ID + ")" + "___Stack " + stankNunber);
        }
    }

    public void ShowComplete(int feet, int coinEarn, bool isMiss, bool isNewHeight, bool isShowPopupUnlockIngredient)
    {
        StartCoroutine(C_ShowComplete(feet, coinEarn, isMiss, isNewHeight, isShowPopupUnlockIngredient));
    }

    private IEnumerator C_ShowComplete(int feet, int coinEarn, bool isMiss, bool isNewHeight, bool isShowPopupUnlockIngredient)
    {
        if (isShowPopupUnlockIngredient)
        {
            UnlockIngredientWhenComplete();
            yield return new WaitForSeconds(2.0f);
        }

        completeUI.ShowComplete(feet, coinEarn, isMiss, isNewHeight);
    }

    public void ShowPopup3Keys()
    {
        popup3keys.SetActive(true);
    }

    public void OnClick_OpenWheel()
    {
        if (isUnlock) return;

        wheelScript.OpenWheel();
        EazySoundManager.PlaySound(SoundManager.Instance.popupOpen);
    }

    public void OnClick_CloseWheel()
    {
        if (isUnlock) return;

        wheelScript.CloseWheel();
        EazySoundManager.PlaySound(SoundManager.Instance.popupClose);
        UpdatePrice();
        CheckMissions();
    }

    public void OnClick_OpenDailyQuest()
    {
        CheckMissions();

        if (isUnlock) return;

        dailyQuestOject.SetActive(true);
        DailyMissionManager.Instance.Load();
        EazySoundManager.PlaySound(SoundManager.Instance.popupOpen);
    }

    public void OnClick_CloseDailyQuest()
    {
        if (isUnlock) return;

        dailyQuestOject.SetActive(false);
        EazySoundManager.PlaySound(SoundManager.Instance.popupClose);
        CheckMissions();
        UpdatePrice();
    }

    public void OnClick_Play()
    {
        if (isUnlock) return;

        HideMainUI();
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
    }

    public void ShowMainUI()
    {
        mainAnimator.SetTrigger("ShowBottom");
        CheckMissions();
    }

    public void HideMainUI()
    {
        mainAnimator.SetTrigger("HideBottom");
    }

    public void CheckMissions()
    {
        int count = 0;
        for (int i = 0; i < DailyMissionManager.Instance.missionsList.Count; i++)
        {
            Mission mission = DailyMissionManager.Instance.missionsList[i];
            if (mission.isCompleted && !mission.isCollected)
            {
                count++;
            }
        }
        imgNotiMission.enabled = count > 0;
    }
}
