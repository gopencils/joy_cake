﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Hellmade.Sound;
using UnityEngine;
using UnityEngine.UI;

public class CompleteUI : MonoBehaviour
{
    public GameObject headerMiss;
    public GameObject headerNeverMiss;
    public GameObject recordObject;
    public GameObject btnCollect;

    public Text feetText;
    public Text coinEarnText;
    private int extraCoin;
    private bool isCollect;

    public void ShowComplete(int feet, int coinEarn, bool isMiss, bool isNewHeight)
    {
        if (isMiss)
        {
            headerMiss.SetActive(true);
            headerNeverMiss.SetActive(false);
        }
        else
        {
            headerMiss.SetActive(false);
            headerNeverMiss.SetActive(true);
        }

        recordObject.SetActive(isNewHeight);
        feetText.text = feet + " FEET";
        coinEarnText.text = coinEarn.ToString();
        extraCoin = coinEarn;
        isCollect = false;

        btnCollect.SetActive(true);
        gameObject.SetActive(true);
    }

    public void OnClick_Collect()
    {
        if (isCollect) return;

        EazySoundManager.PlaySound(SoundManager.Instance.rewardClaimed);
        EazySoundManager.PlaySound(SoundManager.Instance.buttonClick);
        MoreMountains.NiceVibrations.MMVibrationManager.VibrateLight();
        isCollect = true;
        UIManager.Instance.SpawnCoinPS();
        StartCoroutine(C_Collect());
    }

    private IEnumerator C_Collect()
    {
        btnCollect.SetActive(false);
        DataManager.Instance.ChangeCoin((int)(/*Conveyor.Instance.currentCake.fliedIngredientAmount) * 5*/(Shop.Instance.ingredientPrice + Shop.Instance.stackPrice + Shop.Instance.offlineEarningPrice) / 3));
        coinEarnText.DOText("0", 1f, true, ScrambleMode.Numerals);
        // UIManager.Instance.SpawnCoinPS();
        yield return new WaitForSeconds(1.0f);
        Conveyor.Instance.currentCake.dish.transform.DOScaleY(1.3f, 0);
        yield return new WaitForSeconds(0.1f);
        GameManager.Instance.StartGame();
        gameObject.SetActive(false);
    }

}
