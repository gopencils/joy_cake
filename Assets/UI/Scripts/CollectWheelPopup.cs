﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CollectWheelPopup : MonoBehaviour
{
    public Image iconImg;

    public GameObject coinObj;
    public GameObject otherObj;
    public Text coinText;
    public Text otherText;

    public void Init(Sprite _iconSpr,string _descriptionText,bool isCoin)
    {
        if (isCoin)
        {
            coinText.text = _descriptionText;
        }
        else
        {
            otherText.text = _descriptionText;
        }

        coinObj.SetActive(isCoin);
        otherObj.SetActive(!isCoin);

        iconImg.sprite = _iconSpr;

        gameObject.SetActive(true);
    }
}
