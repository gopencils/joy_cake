﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Hellmade.Sound;

public class Wheel : MonoBehaviour
{
    public int SpinNumber
    {
        get
        {
            return PlayerPrefs.GetInt("SpinNumber");
        }
        set
        {
            PlayerPrefs.SetInt("SpinNumber", value);
        }
    }

    public bool isSpin;
    public Transform wheelSlotParent;
    public List<WheelSlot> listWheelSlot = new List<WheelSlot>();
    int slotOrder = 0;
    public CollectWheelPopup collectWheelPopup;
    public GameObject taptospinText;

    public void OpenWheel()
    {
        if (SpinNumber <= 0)
        {
            taptospinText.SetActive(false);
        }
        else
        {
            taptospinText.SetActive(true);
        }

        gameObject.SetActive(true);
    }

    public void OnClick_Spin()
    {
        if (isSpin) return;

        if (SpinNumber <= 0) return;

        SpinNumber -= 1;

        if (SpinNumber <= 0) Shop.Instance.ResetWheelTimer(4);


        Debug.Log(SpinNumber);
        Spin();
    }


    private void Spin()
    {
        // 15% 10coin
        // 13% 50coin
        // 11% 100coin
        // 9%  150coin
        // 7%  200coin
        // 20% key
        // 20% freeslot
        // 5%  ingredient
        // => 55% coin , 20% key , 20% freeslot , 5% ingredient

        isSpin = true;
        float percent = Random.Range(0, 100.0f);

        if (percent < 15.0f)
        {
            slotOrder = 3;
        }
        else if (percent < 28.0f)
        {
            slotOrder = 1;
        }
        else if (percent < 39.0f)
        {
            slotOrder = 5;
        }
        else if (percent < 48.0f)
        {
            slotOrder = 6;
        }
        else if (percent < 55.0f)
        {
            slotOrder = 0;
        }
        else if (percent < 75.0f)
        {
            slotOrder = 4;
        }
        else if (percent < 95.0f)
        {
            slotOrder = 2;
        }
        else
        {
            slotOrder = 7;
        }

        taptospinText.SetActive(false);

        wheelSlotParent.DOLocalRotate(new Vector3(0f, 0f, slotOrder * 45f + 360f * 10f), 5f, RotateMode.FastBeyond360).SetEase(Ease.InOutCirc).OnComplete(Spin_OnComplete);
    }

    private void Spin_OnComplete()
    {
        Debug.Log("Spin Complete");

        // show popup
        if (slotOrder == 2)
        {
            isSpin = false;
            SpinNumber += 1;
            if (SpinNumber <= 0)
            {
                taptospinText.SetActive(false);
            }
            else
            {
                taptospinText.SetActive(true);
            }
        }
        else
        {
            collectWheelPopup.Init(listWheelSlot[slotOrder].mainSprite, listWheelSlot[slotOrder].mainDescription, listWheelSlot[slotOrder].isCoin);
        }
        isSpin = false;
    }

    public void OnClick_Collect()
    {
        EazySoundManager.PlaySound(SoundManager.Instance.coinsCollect);
        listWheelSlot[slotOrder].Collect();
        isSpin = false;

        Debug.Log(SpinNumber + "s");

        if (SpinNumber <= 0)
        {
            taptospinText.SetActive(false);
        }
        else
        {
            taptospinText.SetActive(true);
        }
        Invoke("CloseWheel", 1f);
    }


    private void OnDisable()
    {
        isSpin = false;
    }

    public void CloseWheel()
    {
        Debug.Log("CloseWheel");
        if (isSpin)
            return;
        gameObject.SetActive(false);
    }
}
