﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockIngredient : MonoBehaviour
{
    public Text nameText;
    public Image iconImg;

    public void OnAwake(string _name, Sprite _icon)
    {
        nameText.text = _name + " UNLOCKED";
        iconImg.sprite = _icon;
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    IEnumerator C_PlayOpenSound()
    {
        yield return new WaitForSeconds(0.1f);
        Hellmade.Sound.EazySoundManager.PlaySound(SoundManager.Instance.unlockIngredient);
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        StartCoroutine(C_PlayOpenSound());
    }

    public void Onclick_OK()
    {
        Shop.Instance.ShowIngredientIconAnimationWhenClickOK();
        Shop.Instance.isUnlock = false;
        Shop.Instance.chestAnimation.gameObject.SetActive(false);

        gameObject.SetActive(false);
        Conveyor.Instance.SpawnCakes();

        Shop.Instance.btnPlay.interactable = true;
    }
}
