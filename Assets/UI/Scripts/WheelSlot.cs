﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelSlot : MonoBehaviour
{
    public GameObject coinSlotObject;
    public GameObject keySlotObject;
    public GameObject ingredientSlotObject;
    public GameObject freeSpinSlotObject;

    public Sprite mainSprite;
    public string mainDescription;
    public bool isCoin;

    [Header("Coin")]
    public int coin;
    public Text coinText;

    public Sprite coinSprite;
    public Sprite keySprite;
    public Sprite freeSpinSprite;

    public int ID
    {
        get
        {
            return PlayerPrefs.GetInt("wheelslot_ingredient");
        }
        set
        {
            PlayerPrefs.SetInt("wheelslot_ingredient",value);
        }
    }
    public Image ingredientImg;

    private string IngredientName
    {
        get
        {
            return Shop.Instance.listData[ID].nameIngredient;
        }
    }

    private Sprite IngredientSprite
    {
        get
        {
            return Shop.Instance.listData[ID].iconIngredient;
        }
    }

    public SlotType slotType;

    public enum SlotType
    {
        Coin,
        Key,
        Ingredient,
        FreeSpin
    }

    private void OnEnable()
    {
        UpdateIngredientID();

        int level = PlayerPrefs.GetInt("LEVEL");
        int bonusValue = (level + 1) / 10;
        if (bonusValue < 1)
        {
            bonusValue = 1;
        }
        coin *= (bonusValue);
        isCoin = false;
        coinSlotObject.SetActive(false);
        keySlotObject.SetActive(false);
        ingredientSlotObject.SetActive(false);
        freeSpinSlotObject.SetActive(false);

        switch (slotType)
        {
            case SlotType.Coin:
                SetCoin();
                break;
            case SlotType.Key:
                SetKey();
                break;
            case SlotType.Ingredient:
                SetIngredient();
                break;
            case SlotType.FreeSpin:
                SetFreeSpin();
                break;
        }
    }

    private void UpdateIngredientID()
    {
        if (slotType == SlotType.Ingredient)
        {
            if (Shop.Instance.GetIngredientNumber(ID) == 1)
            {
                int numberLoop = 0;

                for (int i = 0; i < 1; i++)
                {
                    ID = Random.Range(0, Shop.Instance.listData.Count);

                    if (Shop.Instance.GetIngredientNumber(ID) == 1 || Shop.Instance.listData[ID].isChest)
                    {
                        ID = Random.Range(0, Shop.Instance.listData.Count);
                        Debug.Log(ID + " __ " + Shop.Instance.listData.Count);
                        i--;

                        numberLoop++;
                        if (numberLoop >= 200)
                        {
                            slotType = SlotType.Coin;
                            coin = 120;
                            return;
                        }
                    }
                }
            }
        }
    }

    private void SetCoin()
    {
        coinSlotObject.SetActive(true);
        isCoin = true;

        mainSprite = coinSprite;
        coinText.text = coin.ToString();
        mainDescription = coinText.text;
    }

    private void SetKey()
    {
        mainSprite = keySprite;
        keySlotObject.SetActive(true);
        mainDescription = "+1 KEY";
    }

    private void SetIngredient()
    {
        ingredientSlotObject.SetActive(true);

        mainSprite = IngredientSprite;
        ingredientImg.sprite = IngredientSprite;
        mainDescription = IngredientName;

    }

    private void SetFreeSpin()
    {
        mainSprite = freeSpinSprite;
        freeSpinSlotObject.SetActive(true);
        mainDescription = "+1 Spin";
    }

    public void Collect()
    {
        switch (slotType)
        {
            case SlotType.Coin:
                DataManager.Instance.ChangeCoin(coin);
                Debug.Log("coin += " + coin);
                break;
            case SlotType.Key:
                DataManager.Instance.ChangeKey(1);
                Debug.Log("key += 1 __ myKey = " + DataManager.Instance.currentKey);
                break;
            case SlotType.Ingredient:
                Shop.Instance.UnlockIngredientWheel(ID);
                UpdateIngredientID();
                SetIngredient();
                Debug.Log("unlock ingredient    " + ID);
                break;
            case SlotType.FreeSpin:
                Debug.Log("spin += 1");
                Shop.Instance.wheelScript.SpinNumber += 1;
                break;
        }

        
        Shop.Instance.wheelScript.collectWheelPopup.gameObject.SetActive(false);
    }
}
